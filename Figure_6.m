% This skript produces the figure displayed in Result Section 4: 
% Compariosn of combination admonstered at different ratios
% additional figures used in the Supplement: 

% - Figure 6
% - Figures S7; S19-S28
%
% Intialize Matlab:

clc 
clear 
close all

%% Disclaimer: 
% the code shows how the data of the figures can be generated
% the figures used in the manuskript were postprocessed to align panels and
% text more nicely and add differences in line types  

% !!!Generating the data takes long!!! - you can reduce the size of p-vec to run it faster  
%% 1. Plot Settings

dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
green = [150,180,130]/255; 
orange = [200,130,60]/255; 
grey = [150,150,150]/255; 
col1 = dark; 
col2 = orange;
col3 = pink;
col4 = 0.6*ones(1,3);
% Define a color vector 
col_vec = [dark;pink;col4;light;occa;0.2*ones(1,3)];
set(0,'DefaultLineLineWidth',1.5);  
mymap = [grey;light;orange;green;pink;occa;dark];

mymap2 = [lines(7);dark;pink;orange;green;light];
mymap3 = [dark;mymap2(1,:);mymap2(4,:);pink;mymap2(2,:);orange;mymap2(5,:);green;light;mymap2(3,:)];
mymap3 = [repelem(mymap3,10,1);occa];

mymap4_1 = [linspace(dark(1),light(1),20)',linspace(dark(2),light(2),20)',linspace(dark(3),light(3),20)'];
mymap4_2 = [linspace(light(1),1,8)',linspace(light(2),1,8)',linspace(light(3),1,8)'];
mymap4 = [mymap4_1;mymap4_2(2:end,:)];

mymap5_1 =  [linspace(dark(1),mymap4_2(4,1),40)',linspace(dark(2),mymap4_2(4,2),40)',linspace(dark(3),mymap4_2(4,3),40)'];
mymap5_2 = [linspace(pink(1),1,10)',linspace(pink(2),1,10)',linspace(pink(3),1,10)'];
mymap5 = [repmat(dark,[50,1]);mymap5_1;mymap5_2];

mymap6_1 =  [linspace(dark(1),mymap4_2(4,1),8)',linspace(dark(2),mymap4_2(4,2),8)',linspace(dark(3),mymap4_2(4,3),8)'];
mymap6_2 = [linspace(pink(1),1,7)',linspace(pink(2),1,7)',linspace(pink(3),1,7)'];
mymap6 = [mymap6_2;mymap6_1];


%For labeling the panels 
nIDs = 26;
alphabet = ('A':'Z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}
pos = [-0.15,1.25]; 
% then use: 
% text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 

% Fonts
font1 = 9; 
font2 = 11;
font3 = 9; 
%% 2. Parameters: 

% PD Parameter (1x9 struct array) 
% First drug 
PD1.lambda_0 = 0.7;   % replication rate 
PD1.mu_0 = 0.1;        % intrinsic death rate 
PD1.psi_min = -6;     % Psi_min for the first drug 
PD1.kappa = 1;          % Kappa for the first drug 
PD1.zMIC = 1;      % zMIC for the first drug 
PD1.cost = 0.1;         % cost per mutation 
PD1.benefit = 28;       % benefit of mutation 
PD1.p_max = 0.95;        % p_max value for cdial drug action coupled with replication
PD1.sigma_max = 0.95;    % sigma_max value for static drug action 

% Second drug e.g. with different kappa value 
PD2 = PD1; 
PD2.kappa = 2; 

% Third drug e.g. with different kappa value 
PD3 = PD1; 
PD3.kappa = 0.5; 

% Gerenal parameter and vectors for figures 
u_vec = [10^-11,10^-9,10^-5];
N0 = 10^10;
strs_u = {'$u_i = 10^{-11}$','$u_i = 10^{-9}$','$u_i = 10^{-5}$'}; 
strs_mode = {'Bactericidal CK','Bactericidal CR','Bacteriostatic S'};
c_total = linspace(0,35,1000); 
c_total2 = linspace(0,45,1000); 

%% 3. Generate Data 1 - Ratios_B and Ratios_L

p_vec = linspace(0,1,41); 
for i = 1:3 
u = u_vec(i)*ones(1,2); % Mutation rate for each resistance mutation
for k =1:length(p_vec)
for j = 1:length(c_total)
        % Mono Therapies
        % CK 
        mode = [1,1]; 
        Q01 = Extinction_Prob_n_Drugs([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q02 = Extinction_Prob_n_Drugs([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q03 = Extinction_Prob_n_Drugs([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q04 = Extinction_Prob_n_Drugs([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q05 = Extinction_Prob_n_Drugs([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q06 = Extinction_Prob_n_Drugs([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_B.CK{1,i}(k,j) = Q01^(N0);
        Ratios_B.CK{2,i}(k,j) = Q02^(N0);
        Ratios_B.CK{3,i}(k,j) = Q03^(N0);
        Ratios_B.CK{4,i}(k,j) = Q04^(N0);
        Ratios_B.CK{5,i}(k,j) = Q05^(N0);
        Ratios_B.CK{6,i}(k,j) = Q06^(N0);
        
        
        Q01 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q02 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q03 = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q04 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q05 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q06 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_L.CK{1,i}(k,j) = Q01^(N0);
        Ratios_L.CK{2,i}(k,j) = Q02^(N0);
        Ratios_L.CK{3,i}(k,j) = Q03^(N0);
        Ratios_L.CK{4,i}(k,j) = Q04^(N0);
        Ratios_L.CK{5,i}(k,j) = Q05^(N0);
        Ratios_L.CK{6,i}(k,j) = Q06^(N0);
        
        % BActericidal CR 
        mode = [2,2]; 
        Q01 = Extinction_Prob_n_Drugs([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q02 = Extinction_Prob_n_Drugs([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q03 = Extinction_Prob_n_Drugs([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);

        Q04 = Extinction_Prob_n_Drugs([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q05 = Extinction_Prob_n_Drugs([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q06 = Extinction_Prob_n_Drugs([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_B.CR{1,i}(k,j) = Q01^(N0);
        Ratios_B.CR{2,i}(k,j) = Q02^(N0);
        Ratios_B.CR{3,i}(k,j) = Q03^(N0);
        Ratios_B.CR{4,i}(k,j) = Q04^(N0);
        Ratios_B.CR{5,i}(k,j) = Q05^(N0);
        Ratios_B.CR{6,i}(k,j) = Q06^(N0);
        
        
        Q01 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q02 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q03 = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q04 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q05 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q06 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_L.CR{1,i}(k,j) = Q01^(N0);
        Ratios_L.CR{2,i}(k,j) = Q02^(N0);
        Ratios_L.CR{3,i}(k,j) = Q03^(N0);
        Ratios_L.CR{4,i}(k,j) = Q04^(N0);
        Ratios_L.CR{5,i}(k,j) = Q05^(N0);
        Ratios_L.CR{6,i}(k,j) = Q06^(N0);
        
        
         % Bacteriostatic S
        mode = [3,3]; 
        Q01 = Extinction_Prob_n_Drugs([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q02 = Extinction_Prob_n_Drugs([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q03 = Extinction_Prob_n_Drugs([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);

        Q04 = Extinction_Prob_n_Drugs([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q05 = Extinction_Prob_n_Drugs([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q06 = Extinction_Prob_n_Drugs([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_B.S{1,i}(k,j) = Q01^(N0);
        Ratios_B.S{2,i}(k,j) = Q02^(N0);
        Ratios_B.S{3,i}(k,j) = Q03^(N0);
        Ratios_B.S{4,i}(k,j) = Q04^(N0);
        Ratios_B.S{5,i}(k,j) = Q05^(N0);
        Ratios_B.S{6,i}(k,j) = Q06^(N0);
        
        
        Q01 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q02 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q03 = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q04 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q05 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        Q06 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_L.S{1,i}(k,j) = Q01^(N0);
        Ratios_L.S{2,i}(k,j) = Q02^(N0);
        Ratios_L.S{3,i}(k,j) = Q03^(N0);
        Ratios_L.S{4,i}(k,j) = Q04^(N0);
        Ratios_L.S{5,i}(k,j) = Q05^(N0);
        Ratios_L.S{6,i}(k,j) = Q06^(N0);
        
end
end
end

%% 3. Generate Data 2: Ratios_B_sgv and Ratios_L_sgv

% Number of pre-existing mutants 
N0_struct{1} = [N0-1,1,0,0;N0-100,100,0,0;N0-1000000,1000000,0,0]; 
N0_struct{2} = [N0-2,1,1,0;N0-200,100,100,0;N0-2*(999900)-100,999900,999900,100]; 

p_vec = linspace(0,1,41); 
for i = 1:3 
u = u_vec(i)*ones(1,2); % Mutation rate for each resistance mutation
for k =1:length(p_vec)
for j = 1:length(c_total)
        % Mono Therapies
        % CK 
        % Set N0_vec dependening on p: 
        if any([0,1] == p_vec(k)) 
            N0_vec = N0_struct{1};  
        else 
            N0_vec = N0_struct{2}; 
        end
        
        mode = [1,1]; 
        [~,Q01v] = Extinction_Prob_n_Drugs([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q02v] = Extinction_Prob_n_Drugs([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q03v] = Extinction_Prob_n_Drugs([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);

        [~,Q04v] = Extinction_Prob_n_Drugs([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q05v] = Extinction_Prob_n_Drugs([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q06v] = Extinction_Prob_n_Drugs([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_B_sgv.CK{1,i}(k,j) = prod(Q01v.^(N0_vec(i,:)));
        Ratios_B_sgv.CK{2,i}(k,j) = prod(Q02v.^(N0_vec(i,:)));
        Ratios_B_sgv.CK{3,i}(k,j) = prod(Q03v.^(N0_vec(i,:)));
        Ratios_B_sgv.CK{4,i}(k,j) = prod(Q04v.^(N0_vec(i,:)));
        Ratios_B_sgv.CK{5,i}(k,j) = prod(Q05v.^(N0_vec(i,:)));
        Ratios_B_sgv.CK{6,i}(k,j) = prod(Q06v.^(N0_vec(i,:)));
        
        [~,Q01v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q02v]= Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q03v] = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q04v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q05v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q06v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_L_sgv.CK{1,i}(k,j) = prod(Q01v.^(N0_vec(i,:)));
        Ratios_L_sgv.CK{2,i}(k,j) = prod(Q02v.^(N0_vec(i,:)));
        Ratios_L_sgv.CK{3,i}(k,j) = prod(Q03v.^(N0_vec(i,:)));
        Ratios_L_sgv.CK{4,i}(k,j) = prod(Q04v.^(N0_vec(i,:)));
        Ratios_L_sgv.CK{5,i}(k,j) = prod(Q05v.^(N0_vec(i,:)));
        Ratios_L_sgv.CK{6,i}(k,j) = prod(Q06v.^(N0_vec(i,:)));
        
        % BActericidal CR 
        mode = [2,2]; 
        [~,Q01v] = Extinction_Prob_n_Drugs([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q02v] = Extinction_Prob_n_Drugs([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q03v] = Extinction_Prob_n_Drugs([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q04v] = Extinction_Prob_n_Drugs([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q05v] = Extinction_Prob_n_Drugs([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q06v] = Extinction_Prob_n_Drugs([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_B_sgv.CR{1,i}(k,j) = prod(Q01v.^(N0_vec(i,:)));
        Ratios_B_sgv.CR{2,i}(k,j) = prod(Q02v.^(N0_vec(i,:)));
        Ratios_B_sgv.CR{3,i}(k,j) = prod(Q03v.^(N0_vec(i,:)));
        Ratios_B_sgv.CR{4,i}(k,j) = prod(Q04v.^(N0_vec(i,:)));
        Ratios_B_sgv.CR{5,i}(k,j) = prod(Q05v.^(N0_vec(i,:)));
        Ratios_B_sgv.CR{6,i}(k,j) = prod(Q06v.^(N0_vec(i,:)));
        
        [~,Q01v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q02v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q03v] = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q04v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q05v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q06v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_L_sgv.CR{1,i}(k,j) = prod(Q01v.^(N0_vec(i,:)));
        Ratios_L_sgv.CR{2,i}(k,j) = prod(Q02v.^(N0_vec(i,:)));
        Ratios_L_sgv.CR{3,i}(k,j) = prod(Q03v.^(N0_vec(i,:)));
        Ratios_L_sgv.CR{4,i}(k,j) = prod(Q04v.^(N0_vec(i,:)));
        Ratios_L_sgv.CR{5,i}(k,j) = prod(Q05v.^(N0_vec(i,:)));
        Ratios_L_sgv.CR{6,i}(k,j) = prod(Q06v.^(N0_vec(i,:)));
        
        
         % Bacteriostatic S
        mode = [3,3]; 
        [~,Q01v] = Extinction_Prob_n_Drugs([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q02v] = Extinction_Prob_n_Drugs([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q03v] = Extinction_Prob_n_Drugs([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);

        [~,Q04v] = Extinction_Prob_n_Drugs([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q05v] = Extinction_Prob_n_Drugs([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q06v] = Extinction_Prob_n_Drugs([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_B_sgv.S{1,i}(k,j) = prod(Q01v.^(N0_vec(i,:)));
        Ratios_B_sgv.S{2,i}(k,j) = prod(Q02v.^(N0_vec(i,:)));
        Ratios_B_sgv.S{3,i}(k,j) = prod(Q03v.^(N0_vec(i,:)));
        Ratios_B_sgv.S{4,i}(k,j) = prod(Q04v.^(N0_vec(i,:)));
        Ratios_B_sgv.S{5,i}(k,j) = prod(Q05v.^(N0_vec(i,:)));
        Ratios_B_sgv.S{6,i}(k,j) = prod(Q06v.^(N0_vec(i,:)));
        
        [~,Q01v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q02v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q03v] = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q04v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q05v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        [~,Q06v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],mode);
        
        Ratios_L_sgv.S{1,i}(k,j) = prod(Q01v.^(N0_vec(i,:)));
        Ratios_L_sgv.S{2,i}(k,j) = prod(Q02v.^(N0_vec(i,:)));
        Ratios_L_sgv.S{3,i}(k,j) = prod(Q03v.^(N0_vec(i,:)));
        Ratios_L_sgv.S{4,i}(k,j) = prod(Q04v.^(N0_vec(i,:)));
        Ratios_L_sgv.S{5,i}(k,j) = prod(Q05v.^(N0_vec(i,:)));
        Ratios_L_sgv.S{6,i}(k,j) = prod(Q06v.^(N0_vec(i,:)));
        
end
end
end



%% 3. Generate Data - Ratios_B_mod1 and Ratios_L_mod1

% Generate data for the plots 
% All drugs have the same dose response curve with psi_min = -0.065 (set through sigma_max = 0.95) 
PD_2 = PD1;
PD_2.sigma_max = 0.95;  
PD_2.p_max = PD_2.sigma_max/2; 
PD_2.psi_min = PD_2.lambda_0-PD_2.mu_0-PD_2.sigma_max*PD_2.lambda_0;

PD_1 = PD_2; 
PD_3 = PD_2; 


p_vec = linspace(0,1,41); 
for i = 1:3 
u = u_vec(i)*ones(1,2); % Mutation rate for each resistance mutation
for k =1:length(p_vec)
for j = 1:length(c_total)

        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,2]);
        Q02 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,3]);
        Q03 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[2,3]);
        
        Ratios_B_mod1.k1{1,i}(k,j) = Q01^(N0);
        Ratios_B_mod1.k1{2,i}(k,j) = Q02^(N0);
        Ratios_B_mod1.k1{3,i}(k,j) = Q03^(N0);
        
        PD_1.kappa = 2; 
        PD_2.kappa = 2;
        PD_3.kappa = 2;
        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,2]);
        Q02 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,3]);
        Q03 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[2,3]);
        
        Ratios_B_mod1.k2{1,i}(k,j) = Q01^(N0);
        Ratios_B_mod1.k2{2,i}(k,j) = Q02^(N0);
        Ratios_B_mod1.k2{3,i}(k,j) = Q03^(N0);
        
        
        PD_1.kappa = 0.5; 
        PD_2.kappa = 0.5;
        PD_3.kappa = 0.5;
        
        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,2]);
        Q02 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,3]);
        Q03 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[2,3]);
        
        Ratios_B_mod1.k3{1,i}(k,j) = Q01^(N0);
        Ratios_B_mod1.k3{2,i}(k,j) = Q02^(N0);
        Ratios_B_mod1.k3{3,i}(k,j) = Q03^(N0);
        
end
end
end


%% 3. Generate Data - Ratios_B_mod1 and Ratios_L_mod2

% Generate data for the plots 
PD_1 = PD1;
PD_1.p_max = 1; 
PD_1.psi_min = PD_1.lambda_0-PD_1.mu_0-2*PD_1.p_max*PD_1.lambda_0;

PD_2 = PD1;
PD_2.sigma_max = 1;  
PD_2.psi_min = PD_2.lambda_0-PD_2.mu_0-PD_2.sigma_max*PD_2.lambda_0;

PD_3 = PD1; 
PD_3.sigma_max = 1;
PD_3.p_max = PD_3.sigma_max/2; 


p_vec = linspace(0,1,41); 
for i = 1:3 
u = u_vec(i)*ones(1,2); % Mutation rate for each resistance mutation
for k =1:length(p_vec)
for j = 1:length(c_total)

        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,2]);
        Q02 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,3]);
        Q03 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[2,3]);
        
        Ratios_B_mod2.k1{1,i}(k,j) = Q01^(N0);
        Ratios_B_mod2.k1{2,i}(k,j) = Q02^(N0);
        Ratios_B_mod2.k1{3,i}(k,j) = Q03^(N0);
        
        PD_1.kappa = 2; 
        PD_2.kappa = 2;
        PD_3.kappa = 2;
        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,2]);
        Q02 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,3]);
        Q03 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[2,3]);
        
        Ratios_B_mod2.k2{1,i}(k,j) = Q01^(N0);
        Ratios_B_mod2.k2{2,i}(k,j) = Q02^(N0);
        Ratios_B_mod2.k2{3,i}(k,j) = Q03^(N0);
        
        
        PD_1.kappa = 0.5; 
        PD_2.kappa = 0.5;
        PD_3.kappa = 0.5;
        
        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,2]);
        Q02 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[1,3]);
        Q03 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,c_total(j)*[p_vec(k),(1-p_vec(k))],[2,3]);
        
        Ratios_B_mod2.k3{1,i}(k,j) = Q01^(N0);
        Ratios_B_mod2.k3{2,i}(k,j) = Q02^(N0);
        Ratios_B_mod2.k3{3,i}(k,j) = Q03^(N0);
        
end
end
end

%% 3.2: find the maximum ratio for each concentration.
Ratios_B_max.CK = Get_max_Ratio(Ratios_B.CK,p_vec,c_total); 
Ratios_B_max.CR = Get_max_Ratio(Ratios_B.CR,p_vec,c_total); 
Ratios_B_max.S = Get_max_Ratio(Ratios_B.S,p_vec,c_total); 
Ratios_L_max.CK = Get_max_Ratio(Ratios_L.CK,p_vec,c_total); 
Ratios_L_max.CR = Get_max_Ratio(Ratios_L.CR,p_vec,c_total); 
Ratios_L_max.S = Get_max_Ratio(Ratios_L.S,p_vec,c_total); 

Ratios_B_max_sgv.CK = Get_max_Ratio(Ratios_B_sgv.CK,p_vec,c_total); 
Ratios_B_max_sgv.CR = Get_max_Ratio(Ratios_B_sgv.CR,p_vec,c_total); 
Ratios_B_max_sgv.S = Get_max_Ratio(Ratios_B_sgv.S,p_vec,c_total); 
Ratios_L_max_sgv.CK = Get_max_Ratio(Ratios_L_sgv.CK,p_vec,c_total); 
Ratios_L_max_sgv.CR = Get_max_Ratio(Ratios_L_sgv.CR,p_vec,c_total); 
Ratios_L_max_sgv.S = Get_max_Ratio(Ratios_L_sgv.S,p_vec,c_total); 

Ratios_B_max_mod1.k1 = Get_max_Ratio(Ratios_B_mod1.k1,p_vec,c_total); 
Ratios_B_max_mod1.k2 = Get_max_Ratio(Ratios_B_mod1.k2,p_vec,c_total); 
Ratios_B_max_mod1.k3 = Get_max_Ratio(Ratios_B_mod1.k3,p_vec,c_total); 


Ratios_B_max_mod2.k1 = Get_max_Ratio(Ratios_B_mod2.k1,p_vec,c_total); 
Ratios_B_max_mod2.k2 = Get_max_Ratio(Ratios_B_mod2.k2,p_vec,c_total); 
Ratios_B_max_mod2.k3 = Get_max_Ratio(Ratios_B_mod2.k3,p_vec,c_total); 

Ratios_B_max_types.W = Get_max_Ratio(Ratios_B_types.W,p_vec,c_total2); 
Ratios_B_max_types.M = Get_max_Ratio(Ratios_B_types.M,p_vec,c_total2); 
Ratios_B_max_types.M12 = Get_max_Ratio(Ratios_B_types.M12,p_vec,c_total2); 
%% Figure 6
[X,Y] = meshgrid(c_total,p_vec); 
fig1=figure(1);
T=tiledlayout(4,3);
fig1.Units = 'centimeters'; 
fig1.Position=[10,0,20,22]; 

pos = [-0.125,1.15];
for i = 1:3
nexttile 
contourf(X,Y,Ratios_B.CK{3,i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max.CK{3,i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end
xlim([0,5]); 
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    xlim([0,5]); 
    title('$u_i = 10^{-11}, \ \kappa_i = 0.5$','FontSize',font1);
elseif i ==2 
    xlim([0,10]);
    title('$u_i = 10^{-9}, \ \kappa_i = 0.5$','FontSize',font1);
else 
    title('$u_i = 10^{-5}, \ \kappa_i = 0.5$','FontSize',font1);
    xlim([0,35]); 
    cc = colorbar('XTick',[0.5,0.9,1]);
end
end


for i = 1:3
nexttile 
contourf(X,Y,Ratios_B.CK{2,i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max.CK{2,i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end
xlim([0,5]); 
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    xlim([0,5]);  
    title('$u_i = 10^{-11}, \ \kappa_i = 2$','FontSize',font1);
    ylabel('\textsf{Proportion of the {f}irst drug}','Interpreter','latex','FontSize',font1);

elseif i ==2 
    xlim([0,10]);
    title({'$u_i = 10^{-9}, \ \kappa_i = 2$'},'FontSize',font1);
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1);
else 
    xlim([0,35]); 
    title('$u_i = 10^{-5}, \ \kappa_i = 2$','FontSize',font1);
    cc = colorbar('XTick',[0.5,0.9,1]);
end
end



for i = 1:3
nexttile 
contourf(X,Y,Ratios_B.CK{5,i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max.CK{5,i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end
xlim([0,5]); 
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    xlim([0,5]);  
     title('$u_i = 10^{-11}, \ \kappa_1 = 1, \ \kappa_2 = 0.5$','FontSize',font1);
elseif i ==2 
    xlim([0,10]);
    title({'$u_i = 10^{-9}, \ \kappa_1 = 1, \ \kappa_2 = 0.5$'},'FontSize',font1);
else 
    xlim([0,35]); 
  title('$u_i = 10^{-5}, \ \kappa_1 = 1,\  \kappa_2 = 0.5$','FontSize',font1);
    cc = colorbar('XTick',[0.5,0.9,1]);
end
end



for i = 1:3
nexttile 
contourf(X,Y,Ratios_B_mod2.k2{3,i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max_mod2.k2{3,i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end
xlim([0,5]); 
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    xlim([0,5]);  
     title('$u_i = 10^{-11}, \ \kappa_i = 2$','FontSize',font1);
     ylabel('\textsf{Proportion of the {f}irst drug}','Interpreter','latex','FontSize',font1);
elseif i ==2 
    xlim([0,10]);
    title({'$u_i = 10^{-9}, \ \kappa_i = 2$'},'FontSize',font1);
else 
    xlim([0,35]); 
    title('$u_i = 10^{-5}, \ \kappa_i = 2$','FontSize',font1);
    cc = colorbar('XTick',[0.5,0.9,1]);
end
end

title(T,{'\textsf{Bactericidal (CK) drugs with the same Hill coe{f}{f}icient}, $N_{0,M_I}=0$';'\textsf{Bactericidal (CK) drugs with di{f}{f}erent Hill coe{f}{f}icients}, $N_{0,M_I}=0$';'\textsf{Drugs with the same Hill coe{f}{f}icient but di{f}{f}erent modes of action (CR+S)}, $N_{0,M_I}=0$'},'Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% ========================================================================
% SI FIGURES 
% =========================================================================
%% Figures S19-S21 Change the field 'CK' to 'CR' and 'C' to generate the different figures 

str_type = {'$\kappa_1 = \kappa_2 = 1$','$\kappa_1 = \kappa_2 = 2$','$\kappa_1 = \kappa_2 = 0.5$','$\kappa_1 = 1, \kappa_2 = 2$','$\kappa_1 = 1, \kappa_2 = 0.5$','$\kappa_1 = 2, \kappa_2 = 0.5$'};
[X,Y] = meshgrid(c_total,p_vec); 
ind_k = [3,1,2];
fig1=figure(1);

T=tiledlayout(3,3);
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,20,16]; 
pos = [-0,1.1];

for j = 1:3
for i=1:3
nexttile 
% Change the field 'CK' of the variable 'Ratios_B' to either 'CR' or 'S' to
% generate the other figures 
[c,h]=contourf(X,Y,Ratios_B.CK{ind_k(j),i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max.CK{ind_k(j),i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end

if j == 1
    str = strcat(strs_u{i},',','$\ \kappa_i = 0.5$'); 
    title(str,'FontSize',font1); 
elseif j == 2
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$'); 
    title(str,'FontSize',font1); 
else 
    str = strcat(strs_u{i},',','$\ \kappa_i = 2$'); 
    title(str,'FontSize',font1); 
end
if any([3,6] == i)
 cc = colorbar('XTick',[0.5,0.9,1]);
end
set(gca,'FontSize',font1);
if any([2,3,5,6]==i)
    set(gca,'YTicklabels',[]);
end
text(pos(1),pos(2),charlbl{i+(j-1)*3},'Units','normalized','FontSize',font3)
if i == 1
       xlim([0,5]); 
elseif i == 2
    xlim([0,10]);
else
    xlim([0,35]);
end
end
end

 title(T,'\textsf{Bactericidal (CK) drugs with the same Hill coe{f}{f}icient}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
%   title(T,'\textsf{Bactericidal (CR) drugs with the same Hill coe{f}{f}icient}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
%   title(T,'\textsf{Bacteriostatic (S) drugs with the same Hill coe{f}{f}icient}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Proportion of the {f}irst drug}','Interpreter','latex','FontSize',font1);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% Figure S23

str_type = {'$\kappa_1 = \kappa_2 = 1$','$\kappa_1 = \kappa_2 = 2$','$\kappa_1 = \kappa_2 = 0.5$','$\kappa_1 = 1, \kappa_2 = 2$','$\kappa_1 = 1, \kappa_2 = 0.5$','$\kappa_1 = 2, \kappa_2 = 0.5$'};
[X,Y] = meshgrid(c_total,p_vec); 
ind_k = [3,1,2];
fig1=figure(1);
T=tiledlayout(3,3);
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,20,16]; 
pos = [0,1.1];

for j = 1:3
for i=1:3
nexttile 
[c,h]=contourf(X,Y,Ratios_L.CK{ind_k(j),i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_L_max.CK{ind_k(j),i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end


if j == 1
    str = strcat(strs_u{i},',','$\ \kappa_i = 0.5$'); 
    title(str,'FontSize',font1); 
elseif j == 2
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$'); 
    title(str,'FontSize',font1); 
else 
    str = strcat(strs_u{i},',','$\ \kappa_i = 2$'); 
    title(str,'FontSize',font1); 
end
if any([3,6] == i)
 cc = colorbar('XTick',[0.5,0.9,1]);
end
set(gca,'FontSize',font1);
if any([2,3,5,6]==i)
    set(gca,'YTicklabels',[]);
end
text(pos(1),pos(2),charlbl{i+(j-1)*3},'Units','normalized','FontSize',font3)

if i == 1
       xlim([0,5]); 
elseif i == 2
    xlim([0,10]);
else
    xlim([0,35]);
end
end
end

title(T,'\textsf{Bactericidal (CK) drugs with the same Hill coe{f}{f}icient}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
% title(T,'\textsf{Bactericidal (CR)}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
% title(T,'\textsf{Bacteriostatic (S)}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Proportion of the {f}irst drug}','Interpreter','latex','FontSize',font1);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
%% Figures S24-26  Change the field 'CK' to 'CR' and 'C' to generate the different figures 
[X,Y] = meshgrid(c_total,p_vec); 
ind_k = [4,5,6];
fig1=figure(1);
T=tiledlayout(3,3);
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,20,16]; 
pos = [-0.175,1.175];

for j = 1:3
for i=1:3
nexttile 
[c,h]=contourf(X,Y,Ratios_B.CK{ind_k(j),i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max.CK{ind_k(j),i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end

if j == 1
    str = strcat(strs_u{i},',','$\ \kappa_1 = 1, \ \kappa_2 = 2$'); 
    title(str,'FontSize',font1); 
elseif j == 2
    str = strcat(strs_u{i},',','$\ \kappa_1 = 1, \ \kappa_2 = 0.5$'); 
    title(str,'FontSize',font1); 
else 
    str = strcat(strs_u{i},',','$\ \kappa_1 = 2, \ \kappa_2 = 0.5$'); 
    title(str,'FontSize',font1); 
end
if any([3,6] == i)
 cc = colorbar('XTick',[0.5,0.9,1]);
end
set(gca,'FontSize',font1);
if any([2,3,5,6]==i)
    set(gca,'YTicklabels',[]);
end
text(pos(1),pos(2),charlbl{i+(j-1)*3},'Units','normalized','FontSize',font3)

if i == 1
       xlim([0,5]); 
elseif i == 2
    xlim([0,10]);
else
    xlim([0,35]);
end
end
end

 title(T,'\textsf{Bactericidal (CK) drugs with di{f}{f}erent Hill coe{f}{f}icients}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
%  title(T,'\textsf{Bactericidal (CR) drugs with di{f}{f}erent Hill coe{f}{f}icients}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
%   title(T,'\textsf{Bacteriostatic (S) drugs with di{f}{f}erent Hill coe{f}{f}icients}, $N_{0,M_I} = 0$','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Proportion of the {f}irst drug}','Interpreter','latex','FontSize',font1);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% Figure S22 
[X,Y] = meshgrid(c_total,p_vec); 
ind_k = [3,1,2];
fig1=figure(1);
T=tiledlayout(3,3);
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,20,16]; 
pos = [0,1.1];

for j = 1:3
for i=1:3
nexttile 
[c,h]=contourf(X,Y,Ratios_B_sgv.CK{ind_k(j),i},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max_sgv.CK{ind_k(j),i}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end

if j == 1
    str = strcat(strs_u{i},',','$\ \kappa_i = 0.5$'); 
    title(str,'FontSize',font1); 
elseif j == 2
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$'); 
    title(str,'FontSize',font1); 
else 
    str = strcat(strs_u{i},',','$\ \kappa_i = 2$'); 
    title(str,'FontSize',font1); 
end
if any([3,6] == i)
 cc = colorbar('XTick',[0.5,0.9,1]);
end
set(gca,'FontSize',font1);
if any([2,3,5,6]==i)
    set(gca,'YTicklabels',[]);
end
text(pos(1),pos(2),charlbl{i+(j-1)*3},'Units','normalized','FontSize',font3)
if i == 1
       xlim([0,5]); 
elseif i == 2
    xlim([0,10]);
else
    xlim([0,35]);
end
end
end

title(T,'\textsf{Bactericidal (CK) drugs with the same Hill coe{f}{f}icient, presence of pre-existing mutants} ($N_{0,M_I} > 0$)','Interpreter','latex','FontSize',font2);
% title(T,'\textsf{Bactericidal (CR), presence of pre-existing mutants} ($N_{0,M_I} > 0$)','Interpreter','latex','FontSize',font2);
% title(T,'\textsf{Bacteriostatic (S), presence of pre-existing mutants} ($N_{0,M_I} > 0$)','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Proportion of the f irst drug}','Interpreter','latex','FontSize',font1);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
%% Figures S27 and S28 - Change variable name from 'Ratios_B_mod1' to 'Ratios_B_mod2' to generate the other figures 

str_type = {'\textsf{CK+CR}','\textsf{CK+S}','\textsf{CR+S}'};
[X,Y] = meshgrid(c_total,p_vec); 
% kappa = 1

% kappa = 2
fig2=figure(2);
T=tiledlayout(3,3);
T.TileSpacing = 'normal';
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,20,16]; 

for j = 1:3
for i=1:3
nexttile 
[c,h]=contourf(X,Y,Ratios_B_mod1.k2{i,j},[0,linspace(0.5,0.9,5),0.925,0.95,0.975,0.99,1.1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap5)
caxis([0,1])
hold on 
for k = 1:41
plot_h = plot(c_total,Ratios_B_max_mod1.k2{i,j}(k,:),'*','Color',[0.85,0,0],'MarkerSize',2);
end
if any(3==i)
cc = colorbar;
end
if j == 1
    title({str_type{i};'$u_i = 10^{-11}$'}, 'FontSize',font2); 
    xlim([0,5]);
end
if j == 2
    title('$u_i = 10^{-9}$', 'FontSize',font2); 
    xlim([0,10]);
end
if j == 3
    title('$u_i = 10^{-5}$', 'FontSize',font2); 
    xlim([0,35]);
end
set(gca,'FontSize',font1);
if any([2,3,5,6]==i)
    set(gca,'YTicklabels',[]);
end
text(pos(1),pos(2),charlbl{i+(j-1)*3},'Units','normalized','FontSize',font3)
end
end

title(T,'\textsf{Drugs with the same Hill coe{f}{f}icient but di{f}{f}erent modes of action}, $N_{0,M_I} = 0$ ','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Proportion of the f irst drug}','Interpreter','latex','FontSize',font1);
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% ========================================================================
% PD FUNCTIONS 
% =========================================================================
%% GENREATE DATA S7
c_total2 = linspace(0,45,5000); 

% For growth rates of different types 
PD2res = PD2; 
PD2res.zMIC = PD2.zMIC*28;
PD1res = PD1; 
PD1res.zMIC = PD1.zMIC*28;
PD3res = PD3; 
PD3res.zMIC = PD3.zMIC*28;
[X,Y] = meshgrid(c_total2,p_vec); 


PDi = {[PD3,PD3],[PD3res,PD3],[PD3,PD3res],[PD3res,PD3res];[PD2,PD2],[PD2res,PD2],[PD2,PD2res],[PD2res,PD2res];[PD1,PD3],[PD1res,PD3],[PD1,PD3res],[PD1res,PD3res];[PD2,PD2],[PD2res,PD2],[PD2,PD2res],[PD2res,PD2res]};
mode_vec = [1,1;1,1;1,1;2,3];
for k = 1:4
    for l =1:4 
        for i = 1:length(c_total2)
            for j = 1:length(p_vec) 
                Growth_Surf{l,k}(j,i) = psi_action_multiple_drugs(PDi{l,k},[p_vec(j)*c_total2(i);(1-p_vec(j))*c_total2(i)],mode_vec(l,:)); 
                if Growth_Surf{l,k}(j,i)>0
                     Growth_Surf2{l,k}(j,i) = 1; 
                 elseif Growth_Surf{l,k}(j,i)<0
                     Growth_Surf2{l,k}(j,i) = -1;
                 else 
                     Growth_Surf2{l,k}(j,i)=0; 
                 end
            end
        end
    end
end

%% Figure S7
mymap6_1 =  [linspace(dark(1),mymap4_2(4,1),8)',linspace(dark(2),mymap4_2(4,2),8)',linspace(dark(3),mymap4_2(4,3),8)'];
mymap6_2 = [linspace(pink(1),1,30)',linspace(pink(2),1,30)',linspace(pink(3),1,30)'];
mymap6 = [flipud(mymap6_2);mymap6_1];
mymap6 = [repmat([1,1,1],[49,1]);0.2*ones(1,3);repmat(dark,[49,1])];
fig1=figure(1);
T=tiledlayout(4,4);
fig1.Units = 'centimeters'; 
fig1.Position=[10,0,20,22]; 
ind = [3,2,5,7];
pos = [-0.05,1.1];

for i = 1:4
nexttile 
contourf(X,Y,Growth_Surf2{i,1},[-2,0,2],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap6)
% caxis([-6,0.7])
hold on  
text(pos(1),pos(2),charlbl{1+(i-1)*4},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    title({'\textsf{Wild type} ($W$)';'$\kappa_i = 0.5$'},'FontSize',font1); 
elseif i == 2
    title('$\kappa_i = 2$','FontSize',font1);
elseif i == 3
    title({'\textsf{Wild type} ($W$)';'$\kappa_1 = 1, \ \kappa_2 = 0.5$'},'FontSize',font1);
elseif i == 4
    title('$\kappa_i = 2$','FontSize',font1);
end
    xlim([0,3]);

nexttile 
contourf(X,Y,Growth_Surf2{i,2},[-1,0,1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap6)
hold on 
text(pos(1),pos(2),charlbl{2+(i-1)*4},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    title({'\textsf{Single mutant} ($M_1$)';'$\kappa_i = 0.5$'},'FontSize',font1); 
elseif i == 2
    title({'$\kappa_i = 2$'},'FontSize',font1);
elseif i == 3
     title({'\textsf{Single mutant} ($M_1$)';'$\kappa_1 = 1, \ \kappa_2 = 0.5$'},'FontSize',font1);
else
    title({'$\kappa_i = 2$'},'FontSize',font1);
end
    xlim([0,20]);
    
    
nexttile 
contourf(X,Y,Growth_Surf2{i,3},[-1,0,1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap6)
hold on 
text(pos(1),pos(2),charlbl{3+(i-1)*4},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
if i == 1
    title({'\textsf{Single mutant} ($M_2$)';'$\kappa_i = 0.5$'},'FontSize',font1); 
elseif i == 2
    title({'$\kappa_i = 2$'},'FontSize',font1);
elseif i == 3
     title({'\textsf{Single mutant} ($M_2$)';'$\kappa_1 = 1, \ \kappa_2 = 0.5$'},'FontSize',font1);
else
    title({'$\kappa_i = 2$'},'FontSize',font1);
end
    xlim([0,20]);

nexttile 
contourf(X,Y,Growth_Surf2{i,4},[-1,0,1],'Edgecolor',0.2*ones(1,3)); 
colormap(mymap6)
hold on 
text(pos(1),pos(2),charlbl{4+(i-1)*4},'Units','normalized','FontSize',font3)
set(gca,'FontSize',font1); 
cb=colorbar;
cb.Ticks = [-0.9,0,0.9] ; %Create 8 ticks from zero to 1
cb.TickLabels = {'$\psi<0$','$\psi=0$','$\psi>0$'};
% caxis([-6,0.7])
if i == 1
    title({'\textsf{Double mutant} ($M_{1,2}$)';'$\kappa_i = 0.5$'},'FontSize',font1); 
elseif i == 2
    title('$\kappa_i = 2$','FontSize',font1);
elseif i == 3
    title({'\textsf{Double mutant} ($M_{1,2}$)';'$\kappa_1 = 1, \ \kappa_2 = 0.5$'},'FontSize',font1);
elseif i == 4
    title('$\kappa_i = 2$','FontSize',font1);
end
end


title(T,{'\textsf{Bactericidal (CK) drugs with the same Hill coe{f}{f}icient}, $N_{0,M_I}=0$';'\textsf{Bactericidal (CK) drugs with di{f}{f}erent Hill coe{f}{f}icients}, $N_{0,M_I}=0$';'\textsf{Drugs with the same Hill coe{f}{f}icient but di{f}{f}erent modes of action (CR+S)}, $N_{0,M_I}=0$'},'Interpreter','latex','FontSize',font2);

xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Proportion of the {f}irst drug}','Interpreter','latex','FontSize',font1);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


