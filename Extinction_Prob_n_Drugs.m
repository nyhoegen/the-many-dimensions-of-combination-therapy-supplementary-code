function [Q,Qi_out] = Extinction_Prob_n_Drugs(PD,u,c,mode)
% Function to calculate the extinction probability of combination
% therapy with constant drug concentration for variable number of drugs
% Based on BLISS INDEPNDENCE: 
% 
% 
% INPUT 
% PD      = nx9 struct array discribing the PD charavteristics of the
%            drugs and the different types (W MA MB MC MAB MAC MBC MABC)
%            lambda_0 - replication rate 
%            mu_0     - intrinsic death rate 
%            zMIC - susceptibility towards the drug 
%            kappa - steepness of the pharamcodynamic function 
%            psi_min - max kill rate of bactericidal (kill) drug
%            p_max - max drug effect of bactericidal (growth) drug 
%            sigma_max - max drug effect of bacteriostatic drug 
%            benefit - benefit of resistance
%            cost - cost of resistance 
% c        = 1xndrug concnetration for the two drugs
% mode     = 1xn mode of action (e.g. 1-3 as above) 
% u     = 1xn vector with mutation probabilities (uA,uB)
% 
% OUTPUT 
% P_ext = extinction probability of the process 
%
%==========================================================================
% Number of drugs 
nd = length(c); 
% Number of Types 
nt = 2^(nd); 
% Initialize Vector for Extinction probabilities starting with different
% types 
Qi = zeros(1,nt); 

%==========================================================================
% FULLY RESISTANT TYPE
%==========================================================================
% Calculate the birth and death probabilities 
[rho_ti,delta_ti] = birth_death_prob(PD,c,mode,ones(1,nd));

Qti_1 = 1/(2*rho_ti) + sqrt((1/(2*rho_ti))^2-(delta_ti)/rho_ti);
Qti_2 = 1/(2*rho_ti) - sqrt((1/(2*rho_ti))^2-(delta_ti)/rho_ti);
Qti = min([Qti_1,Qti_2,1]);

% save in array 
Qi(1,1) = Qti; 

%==========================================================================
% OTHER TYPES 
%==========================================================================

% Generate matrix with all possible resistant types (binary matrix) 
MT_base = ff2n(nd); 
MT = []; 
% order matrix elements 
for i = nd:-1:0
    ind_vec = sum(MT_base,2)==i; 
    MT = [MT;MT_base(ind_vec,:)];
end

for i = 2:nt
    % Calculate the rates and probabilities for birth and death 
    [rho_ti,delta_ti] = birth_death_prob(PD,c,mode,MT(i,:));
    
    % Calculate p and q for pq-formular: 
    q = delta_ti/(rho_ti*prod(1-u((MT(1,:)-MT(i,:))==1))); 
    
    % find all possible mutants that can be generates: 
    n_res = sum(MT(i,:)); 
    % all types with more mutations 
    ind_more_res = sum(MT,2)>n_res; 
    MT_possible_cand = MT(ind_more_res,:); 
    [ncand,~] = size(MT_possible_cand); 
    MT_possible = []; 
    % making sure every candidtate mutatnt has the mutation of the type we
    % are considering here.
    for k=1:ncand
        diff= MT_possible_cand(k,:)-MT(i,:); 
        if ~any(diff<0)
            MT_possible = [MT_possible;MT_possible_cand(k,:)];
        end
            
    end
    
    % calculate the sum of all products of extinction probability of the
    % possible mutants and their mutation rate
    
    sum_vals = [];   
    [nM,~] = size(MT_possible); 
    for j=1:nM
        new_mut = MT_possible(j,:)-MT(i,:); 
        not_mut = ~MT_possible(j,:); 
      
        ind_q = find(ismember(MT,MT_possible(j,:),'rows')); 
        if sum(not_mut == 1) > 0
            sum_vals(1,j) = Qi(1,ind_q)*prod(u(1,new_mut==1))*prod(1-u(not_mut));
        else 
            sum_vals(1,j) = Qi(1,ind_q)*prod(u(1,new_mut==1));
        end
    end
    
    p = (rho_ti*sum(sum_vals)-1)/(rho_ti*prod(1-u((MT(1,:)-MT(i,:))==1)));
    
    % Solve pg-formular
    Qti_1 = -p/2 + sqrt((p/2)^2-q); 
    Qti_2 = -p/2 - sqrt((p/2)^2-q);   
    
    % Exinction probability is smallest root in [0,1]
    Qti = min([Qti_1,Qti_2,1]); 
    Qi(1,i)=Qti; 
end

Q = Qi(end); 

% output all Probabilities 
Qi_out = fliplr(Qi); 

end

function [rho,delta] = birth_death_prob(PD,c,mode,ti_vec)
% Function to calulate the brith and death rates and their respective
% probabilities for the type with vec ti_vec 
nd = length(c);

    PD_temp = PD; 
    % Implement growth costs: 
    lambda_new = PD(1).lambda_0; 
    for i = 1:nd
        if ti_vec(i) == 1
            lambda_new = lambda_new*(1-PD_temp(i).cost); 
        end
    end
    % Update lambda_0 in every struct array and zMIC value 
    for i = 1:nd 
        PD_temp(i).lambda_0 = lambda_new; 
        if ti_vec(i) == 1
            PD_temp(i).zMIC = PD(i).zMIC*PD(i).benefit;
        end
    end

    % Calculate the brith and death rates
    [~,r0_ti,di_ti] = psi_action_multiple_drugs(PD_temp,c,mode);
    % Probabilities 
    rho = r0_ti/(r0_ti+di_ti);
    delta = di_ti/(r0_ti+di_ti);
end
