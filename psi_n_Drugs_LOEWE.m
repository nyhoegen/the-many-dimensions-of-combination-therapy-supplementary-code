function [f_i1] = psi_n_Drugs_LOEWE(PD,c,mode)
% Function to calculate the growth rate for the treatment with n different
% drugs under the assumption of Loewe additivity 
% Only Bactericidal drug effect on the kill rate 
% 
% INPUT 
% PD       = 1xn struct array discribing the PD charavteristics of the drugs
%            lambda_0 - replication rate (equal for all drugs)
%            mu_0     - intrinsic death rate (equal for all drugs) 
%            zMIC - susceptibility towards the drug 
%            kappa - steepness of the pharamcodynamic function 
%            psi_min - max kill rate of bactericidal (kill) drug
%            p_max - max drug effect of bactericidal (growth) drug 
%            sigma_max - max drug effect of bacteriostatic drug 
% c        = 1xn drug vector of drug concnetration for all drugs
% mode     = mode of action (needs to be the same for the two drugs
% 
% OUTPUT 
% psi      = growth rate of the bacterial population 
% 
%==========================================================================
nd = length(c);

% define function handles for both drug PDs
% differentiate between the differet modes of action: 
if mode(1,1) == 1 % Bactericidal independent of replication 
    psi = @(c,PDi) (PDi.lambda_0-PDi.mu_0)-(PDi.lambda_0-PDi.mu_0-PDi.psi_min)*(c/PDi.zMIC).^PDi.kappa./((c/PDi.zMIC).^PDi.kappa-PDi.psi_min/(PDi.lambda_0-PDi.mu_0)); 
    inv_psi =  @(fc,PDi) ((PDi.psi_min*(PDi.lambda_0-PDi.mu_0-fc))./((PDi.lambda_0-PDi.mu_0)*(PDi.psi_min-fc))).^(1/PDi.kappa)*PDi.zMIC; 
elseif mode(1,1) == 2 % Bactericidal dependent of replication
    psi = @(c,PDi) (PDi.lambda_0-PDi.mu_0)-(PDi.lambda_0*2*PDi.p_max*(c/PDi.zMIC).^PDi.kappa)./((c/PDi.zMIC).^PDi.kappa-1+(2*PDi.p_max*PDi.lambda_0)/(PDi.lambda_0-PDi.mu_0)); 
    inv_psi = @(fc,PDi) PDi.zMIC*(((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*2*PDi.p_max)*(PDi.lambda_0-PDi.mu_0-fc))/((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*2*PDi.p_max-fc)*(PDi.lambda_0-PDi.mu_0))).^(1/PDi.kappa);
elseif mode(1,1) == 3 %Becteriostatic
    psi = @(c,PDi) (PDi.lambda_0-PDi.mu_0)-(PDi.lambda_0*PDi.sigma_max*(c/PDi.zMIC).^PDi.kappa)./((c/PDi.zMIC).^PDi.kappa-1+(PDi.sigma_max*PDi.lambda_0)/(PDi.lambda_0-PDi.mu_0)); 
    inv_psi = @(fc,PDi) PDi.zMIC*(((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*PDi.sigma_max)*(PDi.lambda_0-PDi.mu_0-fc))/((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*PDi.sigma_max-fc)*(PDi.lambda_0-PDi.mu_0))).^(1/PDi.kappa);
end 


% calculate the equivalent concentrations using function 1 (first drug) 
x_eq = 0; 
for i = 2:nd 
    x_temp = inv_psi(psi(c(1,i),PD(i)),PD(1)); 
    x_eq = x_eq+x_temp; 
end

%Calculate the growth rate according to loewe 
f_i1 = psi(c(1,1)+x_eq,PD(1)); 

end