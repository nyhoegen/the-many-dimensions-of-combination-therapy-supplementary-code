% This skript produces the figure displayed in Result Section 1: 
% Compariosn of mono-therapies for different modes of action 

% - Figure 2 

% Intialize Matlab:
clc
clear all 
close all 

%% Disclaimer: 
% the code shows how the data of the figures can be generated
% the figures used in the manuskript were postprocessed to align panels and
% text more nicely add differences in line types 
% for this patricular figure, the figure parts were produces separately and
% combined afterwards

%% 1. Plot Settings

dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
green = [150,180,130]/255; 
orange = [200,130,60]/255; 
col1 = dark; 
col2 = light;
col3 = pink;
grey = 0.8*ones(1,3);
% Define a color vector 
set(0,'DefaultLineLineWidth',1.5);  

% For labeling the panels 
nIDs = 12;
alphabet = ('A':'Z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}
pos = [-0.15,1.25]; 
% then use: 
% text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',12) 
% Fonts
font1 = 9; 
font2 = 11;
font3 = 9; 
%% 2. Parameters: 

% PD Parameter (1x9 struct array) 
% First drug 
PD1.lambda_0 = 0.7;   % replication rate 
PD1.mu_0 = 0.1;        % intrinsic death rate 
PD1.psi_min = -6;     % Psi_min for the first drug 
PD1.kappa = 1;          % Kappa for the first drug 
PD1.zMIC = 1;      % zMIC for the first drug 
PD1.cost = 0.1;         % cost per mutation 
PD1.benefit = 28;       % benefit of mutation 
PD1.p_max = 0.45;        % p_max value for cdial drug action coupled with replication
PD1.sigma_max = 0.95;    % sigma_max value for static drug action 

% Second drug e.g. with different kappa value 
PD2 = PD1; 
PD2.kappa = 2; 

% Third drug e.g. with different kappa value 
PD3 = PD1; 
PD3.kappa = 0.5; 

% Vector for different mutation probabilities 
u_vec = [10^-11,10^-9,10^-5];
% Initial population size 
N0 = 10^10;
% Strings for the figures 
strs_u = {'$u_A = u_B = 10^{-11}$','$u_A = u_B = 10^{-9}$','$u_A = u_B = 10^{-5}$'}; 
strs_T = {'CK1','CK2','CR','S','CK1+CK1','CK2+CK2','CR+CR','S+S','CK1+CK2','CK1+CR','CK1+S','CK2+CR','CK2+S','CR+S','S+S','CR+CR','CK+CK','S+CR','S+CK','CR+CK','S','CR','CK'};
% Vector of drug concnetrations
c_total = linspace(0,35,1000); 

% Number of cell in standing genetic variation: 
% rows for the different mutation rates 
N0_vec = [N0-1,1,0,0;N0-100,100,0,0;N0-1000000,1000000,0,0];
%% 3. Fitting the psi min values to achive same dose-response curve
% First set - match all dose-response curves to that of the bacteriostatic
% drug: 
PD_1 = PD1;
PD_1.psi_min = PD_1.lambda_0-PD_1.mu_0-PD_1.sigma_max*PD_1.lambda_0;
PD_1.p_max = PD_1.sigma_max/2; 


% Second set - match the bacteriostatic drug independent of replication to 
% that acting during replication: 
PD_2 = PD1;
PD_2.psi_min = PD_2.lambda_0-PD_2.mu_0-2*PD_2.p_max*PD_2.lambda_0;

% CK better than S and CR
PD_3 = PD1; 
PD_3.psi_min = -5; % -2 for match above (needs to be below -1.3)
PD_3.p_max = 0.99;  % 0.8 for match above (needs to be larger than 0.7037)
PD_3.s_max = 0.95;

%% 4. Generate Data: 

for i = 1:3 
u = u_vec(i)*ones(1,2);      % Mutation rate for each resistance mutation
for j = 1:length(c_total)
        % Mono Therapies 
        Q01 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[1,1]);
        Q02 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[2,2]);
        Q03 = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[3,3]);
        
        Q04 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,[c_total(j),0],[1,1]);
        Q05 = Extinction_Prob_n_Drugs([PD_2,PD_2],u,[c_total(j),0],[2,2]);
        
        Q06 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,[c_total(j),0],[1,1]);
        Q07 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,[c_total(j),0],[2,2]);
        Q08 = Extinction_Prob_n_Drugs([PD_3,PD_3],u,[c_total(j),0],[3,3]);
         
        Q0_Mode{1,i}(1,j) = Q01^(N0);
        Q0_Mode{2,i}(1,j) = Q02^(N0); 
        Q0_Mode{3,i}(1,j) = Q03^(N0);
        Q0_Mode{4,i}(1,j) = Q04^(N0);
        Q0_Mode{5,i}(1,j) = Q05^(N0);
        Q0_Mode{6,i}(1,j) = Q06^(N0);
        Q0_Mode{7,i}(1,j) = Q07^(N0);
        Q0_Mode{8,i}(1,j) = Q08^(N0);
         
        % Standing genetic variation: 
        [~,Q01v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[1,1]);
        [~,Q02v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[2,2]);
        [~,Q03v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[3,3]);
        [~,Q04v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,[c_total(j),0],[1,1]);
        [~,Q05v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,[c_total(j),0],[2,2]);
        [~,Q06v] = Extinction_Prob_n_Drugs([PD_3,PD_3],u,[c_total(j),0],[1,1]);
        [~,Q07v] = Extinction_Prob_n_Drugs([PD_3,PD_3],u,[c_total(j),0],[2,2]);
        [~,Q08v] = Extinction_Prob_n_Drugs([PD_3,PD_3],u,[c_total(j),0],[3,3]);
        
        Q0_Mode_sgv{1,i}(1,j) = prod(Q01v.^(N0_vec(i,:)));
        Q0_Mode_sgv{2,i}(1,j) = prod(Q02v.^(N0_vec(i,:)));
        Q0_Mode_sgv{3,i}(1,j) = prod(Q03v.^(N0_vec(i,:)));
        Q0_Mode_sgv{4,i}(1,j) = prod(Q04v.^(N0_vec(i,:)));
        Q0_Mode_sgv{5,i}(1,j) = prod(Q05v.^(N0_vec(i,:)));
        Q0_Mode_sgv{6,i}(1,j) = prod(Q06v.^(N0_vec(i,:)));
        Q0_Mode_sgv{7,i}(1,j) = prod(Q07v.^(N0_vec(i,:)));
        Q0_Mode_sgv{8,i}(1,j) = prod(Q08v.^(N0_vec(i,:)));
         
        % Data for extinction Approximation
        [p_est1,N_M1] = Extinction_Approx_n_Drugs(PD_1,u,c_total(j),1,N0); 
        [p_est2,N_M2] = Extinction_Approx_n_Drugs(PD_1,u,c_total(j),2,N0);
        [p_est3,N_M3] = Extinction_Approx_n_Drugs(PD_1,u,c_total(j),3,N0); 
        
        [p_est4,N_M4] = Extinction_Approx_n_Drugs(PD_2,u,c_total(j),1,N0); 
        [p_est5,N_M5] = Extinction_Approx_n_Drugs(PD_2,u,c_total(j),2,N0); 
        
        [p_est6,N_M6] = Extinction_Approx_n_Drugs(PD_3,u,c_total(j),1,N0); 
        [p_est7,N_M7] = Extinction_Approx_n_Drugs(PD_3,u,c_total(j),2,N0); 
        [p_est8,N_M8] = Extinction_Approx_n_Drugs(PD_3,u,c_total(j),3,N0);
        
        p_est{1,i}(:,j) = p_est1'; 
        p_est{2,i}(:,j) = p_est2'; 
        p_est{3,i}(:,j) = p_est3'; 
        
        p_est{4,i}(:,j) = p_est4';  
        p_est{5,i}(:,j) = p_est5'; 
        
        p_est{6,i}(:,j) = p_est6';  
        p_est{7,i}(:,j) = p_est7';
        p_est{8,i}(:,j) = p_est8';
        
        N_M{1,i}(:,j) = N_M1';
        N_M{2,i}(:,j) = N_M2';
        N_M{3,i}(:,j) = N_M3';
       
        N_M{4,i}(:,j) = N_M4';
        N_M{5,i}(:,j) = N_M5';
        
        N_M{6,i}(:,j) = N_M6';
        N_M{7,i}(:,j) = N_M7';
        N_M{8,i}(:,j) = N_M8';
        

end
end

%% 5. Plot data - Figure 2

u_ind =1;
pos = [-0.22,1.15];
fig1=figure(1); 
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,15.5,10]; 
T = tiledlayout(2,3);  

% Match 1 - probability of treatment success 
nexttile
plot(1+c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_1,c,1),c_total),'Color',dark); 
hold on 
plot(1+c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_1,c,2),c_total),'-.','Color',green); 
hold on
plot(1+c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_1,c,3),c_total),'--','Color',pink); 
hold on
set(gca,'FontSize',font1);
ylabel('$\psi_W$'); 
set(gca,'XScale','log'); 
set(gca,'XTick',[1,2,11,101],'XTickLabels',{'0','1','10','100'});
ylim([-0.3,0.8]);
text(pos(1),pos(2),charlbl{1},'Units','normalized','FontSize',font3) 
leg3=legend('CK','CR','S','Location','northeast','Interpreter','latex');
leg3.ItemTokenSize = [15,18];
title({'\textsf{Probability of treatment success}';'\textsf{Dose response curves}'},'FontSize',font1)

nexttile
plot(1+c_total,Q0_Mode{1,u_ind},'Color',dark);
hold on  
plot(1+c_total,Q0_Mode{2,u_ind},'Color',green);
hold on 
plot(1+c_total,Q0_Mode{3,u_ind},'Color',pink);
hold on 
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'XScale','log'); 
ylabel('$P_{\mathrm{success}}$');
text(pos(1),pos(2),charlbl{2},'Units','normalized','FontSize',font3) 
title('$N_{0,M}=0, u = 10^{-11}$','FontSize',font1); 

nexttile
plot(1+c_total,Q0_Mode_sgv{1,u_ind},'Color',dark);
hold on  
plot(1+c_total,Q0_Mode_sgv{2,u_ind},'Color',green);
hold on
plot(1+c_total,Q0_Mode_sgv{3,u_ind},'Color',pink);
hold on 
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'XScale','log'); 
% text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3) 
title('$N_{0,M}=1,u=10^{-11}$','FontSize',font1); 
ylabel('$P_{\mathrm{success}}$');

% Match 1 - Approximation-------------------------------------------------------
nexttile 
plot(c_total+1,p_est{1,u_ind}(2,:),'Color',dark); 
hold on 
plot(c_total+1,p_est{2,u_ind}(2,:),'-.','Color',green); 
hold on
plot(c_total+1,p_est{3,u_ind}(2,:),'--','Color',pink); 
hold on
set(gca,'FontSize',10); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3) 
ylabel('$p_{\mathrm{est}}$'); 
title('\textsf{Establishment probability}','Interpreter','latex','FontSize',font1)


nexttile 
plot(c_total+1,N_M{1,u_ind}(2,:),'Color',dark); 
hold on 
plot(c_total+1,N_M{2,u_ind}(2,:),'-.','Color',green); 
hold on
plot(c_total+1,N_M{3,u_ind}(2,:),'--','Color',pink); 
hold on
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'YTick',[0.1,1,10^1,10^2,10^3],'YTickLabels',{' ','1',' ',' ','$10^3$'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
% text(pos(1),pos(2),charlbl{5},'Units','normalized','FontSize',font3) 
ylabel('$N_{M}^{\mathrm{(tot)}}$'); 
ylim([0.01,10^4])
title({'\textsf{Components of the approximation,} $u_i = 10^{-11}$','\textsf{Number of de novo mutants}'},'FontSize',font1)


nexttile 
plot(c_total+1,p_est{1,u_ind}(2,:).*N_M{1,2}(2,:),'Color',dark); 
hold on 
plot(c_total+1,p_est{2,u_ind}(2,:).*N_M{2,2}(2,:),'-.','Color',green); 
hold on
plot(c_total+1,p_est{3,u_ind}(2,:).*N_M{3,2}(2,:),'--','Color',pink); 
hold on
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'YTick',[0.1,1,10^1,10^2,10^3],'YTickLabels',{' ','1',' ',' ','$10^3$'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
title('\textsf{Number of successful mutants}','Interpreter','latex','FontSize',font1)
% text(pos(1),pos(2),charlbl{6},'Units','normalized','FontSize',font3) 
ylabel('$p_{\mathrm{est}} \cdot N_{M}^{\mathrm{(tot)}}$'); 
ylim([0.01,10^4])

xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,'\textsf{Identical dose response curves} ($\psi_{\mathrm{min}}^{\mathrm{S}}=\psi_{\mathrm{min}}^{\mathrm{CR}}=\psi_{\mathrm{min}}^{\mathrm{CK}} = -0.065h^{-1}$)','interpreter','latex','Fontsize',font2);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','XLim'),'XLim',[1,35]);

fig2=figure(2); 
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,10]; 
T = tiledlayout(2,3);  
% Match 1 - probability of treatment success 
nexttile
plot(1+c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_3,c,1),c_total),'Color',dark); 
hold on 
plot(1+c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_3,c,2),c_total),'Color',green); 
hold on
plot(1+c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_3,c,3),c_total),'Color',pink); 
hold on
set(gca,'FontSize',font1);
ylabel('$\psi_W$'); 
set(gca,'XScale','log'); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
ylim([-5,0.8]);
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3) 
leg3=legend('CK','CR','S','Location','southwest','Interpreter','latex');
leg3.ItemTokenSize = [5,18];
title({'\textsf{Probability of treatment success}';'\textsf{Dose response curves}'},'FontSize',font1)


nexttile
plot(1+c_total,Q0_Mode{6,u_ind},'Color',dark);
hold on  
plot(1+c_total,Q0_Mode{7,u_ind},'Color',green);
hold on 
plot(1+c_total,Q0_Mode{8,u_ind},'Color',pink);
hold on 
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'XScale','log'); 
ylabel('$P_{\mathrm{success}}$');
text(pos(1),pos(2),charlbl{5},'Units','normalized','FontSize',font3) 
title('$N_{0,M}=0, u = 10^{-11}$','FontSize',font1); 

nexttile
plot(1+c_total,Q0_Mode{6,2},'Color',dark);
hold on  
plot(1+c_total,Q0_Mode{7,2},'Color',green);
hold on
plot(1+c_total,Q0_Mode{8,2},'Color',pink);
hold on 
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'XScale','log'); 
% text(pos(1),pos(2),charlbl{9},'Units','normalized','FontSize',font3) 
title('$N_{0,M}=1,u=10^{-9}$','FontSize',font1); 
ylabel('$P_{\mathrm{success}}$');

% Match 1 - Approximation-------------------------------------------------------
nexttile 
plot(c_total+1,p_est{6,u_ind}(2,:),'Color',dark); 
hold on 
plot(c_total+1,p_est{7,u_ind}(2,:),'Color',green); 
hold on
plot(c_total+1,p_est{8,u_ind}(2,:),'Color',pink); 
hold on
set(gca,'FontSize',font1); 
set(gca,'XTick',[1,2,11],'XTickLabels',{'0','1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{6},'Units','normalized','FontSize',font3) 
ylabel('$p_{\mathrm{est}}$'); 
title('\textsf{Establishment probability}','Interpreter','latex','FontSize',font1)


nexttile 
plot(c_total+1,N_M{6,u_ind}(2,:),'Color',dark); 
hold on 
plot(c_total+1,N_M{7,u_ind}(2,:),'Color',green); 
hold on
plot(c_total+1,N_M{8,u_ind}(2,:),'Color',pink); 
hold on
set(gca,'FontSize',font1); 
set(gca,'YTick',[0.1,1,10^1,10^2],'YTickLabels',{' ','1',' ','$10^2$'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
% text(pos(1),pos(2),charlbl{11},'Units','normalized','FontSize',font3) 
ylabel('$N_{M}^{\mathrm{(tot)}}$'); 
ylim([0.001,10^4])
title({'\textsf{Components of the approximation,} $u_i = 10^{-11}$','\textsf{Number of de novo mutants}'},'FontSize',font1)

nexttile 
plot(c_total+1,p_est{6,u_ind}(2,:).*N_M{6,2}(2,:),'Color',dark); 
hold on 
plot(c_total+1,p_est{7,u_ind}(2,:).*N_M{7,2}(2,:),'Color',green); 
hold on
plot(c_total+1,p_est{8,u_ind}(2,:).*N_M{8,2}(2,:),'Color',pink); 
hold on
set(gca,'FontSize',font1); 
set(gca,'YTick',[0.1,1,10^1,10^2],'YTickLabels',{' ','1',' ','$10^2$'});
set(gca,'XScale','log'); 
set(gca,'YScale','log');  
title('\textsf{Number of successful mutants}','Interpreter','latex','FontSize',font1)
% text(pos(1),pos(2),charlbl{12},'Units','normalized','FontSize',font3) 
ylabel('$p_{\mathrm{est}} \cdot N_{M}^{\mathrm{(tot)}}$'); 
ylim([0.001,10^4])



xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,'\textsf{Di{f}{f}erent dose response curves} ($\psi_{\mathrm{min}}^{\mathrm{S}} = -0.065h^{-1},\psi_{\mathrm{min}}^{\mathrm{CR}} = -0.786h^{-1},\ \psi_{\mathrm{min}}^{\mathrm{CK}} = -5h^{-1}$)','interpreter','latex','Fontsize',font2);
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','XLim'),'XLim',[1,35]);
