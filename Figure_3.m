% This skript produces the figure displayed in Result Section 2: 
% Compariosn of mono-therapies with two-drug combinations and
% additional figures used in the Supplement: 

% - Figure 3 
% - Figures S4-S6; S10-S14 
%
% Intialize Matlab:
clc
clear all 
close all 

%% Disclaimer: 
% the code shows how the data of the figures can be generated
% the figures used in the manuskript were postprocessed to align panels and
% text more nicely and add differences in line types 

%% 1. Plot Settings

dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
green = [150,180,130]/255; 
orange = [200,130,60]/255; 
col1 = dark; 
col2 = pink;
col3 = green;
mymap = [dark;green;pink];
grey = 0.8*ones(1,3);
% Define a color vector 
set(0,'DefaultLineLineWidth',1.5);  

% For labeling the panels 
nIDs = 12;
alphabet = ('A':'Z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}
pos = [-0.15,1.25]; 
% then use: 
% text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
% Fonts
font1 = 9; 
font2 = 11;
font3 = 9; 
%% 2. Parameters: 

% PD Parameter (1x9 struct array) 
% First drug 
PD1.lambda_0 = 0.7;   % replication rate 
PD1.mu_0 = 0.1;        % intrinsic death rate 
PD1.psi_min = -6;     % Psi_min for the first drug 
PD1.kappa = 1;          % Kappa for the first drug 
PD1.zMIC = 1;      % zMIC for the first drug 
PD1.cost = 0.1;         % cost per mutation 
PD1.benefit = 28;       % benefit of mutation 
PD1.p_max = 0.95;        % p_max value for cdial drug action coupled with replication
PD1.sigma_max = 0.95;    % sigma_max value for static drug action 

% Second drug e.g. with different kappa value 
PD2 = PD1; 
PD2.kappa = 2; 

% Third drug e.g. with different kappa value 
PD3 = PD1; 
PD3.kappa = 0.5; 

% Vector for different mutation probabilities 
u_vec = [10^-11,10^-9,10^-5];
% Initial population size 
N0 = 10^10;
% Strings for the figures 
strs_u = {'$\ u_i = 10^{-11}$','$\ u_i = 10^{-9}$','$\ u_i = 10^{-5}$'}; 
strs_T = {'CK1','CK2','CR','S','CK1+CK1','CK2+CK2','CR+CR','S+S','CK1+CK2','CK1+CR','CK1+S','CK2+CR','CK2+S','CR+S','S+S','CR+CR','CK+CK','S+CR','S+CK','CR+CK','S','CR','CK'};
% Vector of drug concnetrations
c_total = linspace(0,40,1000); 


N0_struct = {}; 
N0_struct{1} = [N0-1,1;N0-100,100;N0-1000000,1000000]; 
N0_struct{2} = [N0-2,1,1,0;N0-200,100,100,0;N0-2*(999900)-100,999900,999900,100]; 
%% 3. Generate Data for all possible Combinations:

for i = 1:3 
u = u_vec(i)*ones(1,2);      % Mutation rate for each resistance mutation
for j = 1:length(c_total)
        % Mono Therapies
        % CK 
        Q01 = Extinction_Prob_n_Drugs(PD1,u(1,1),c_total(j),1);
        Q02 = Extinction_Prob_n_Drugs(PD2,u(1,1),c_total(j),1);
        Q03 = Extinction_Prob_n_Drugs(PD3,u(1,1),c_total(j),1);
        % CR 
        Q04 = Extinction_Prob_n_Drugs(PD1,u(1,1),c_total(j),2);
        Q05 = Extinction_Prob_n_Drugs(PD2,u(1,1),c_total(j),2);
        Q06 = Extinction_Prob_n_Drugs(PD3,u(1,1),c_total(j),2);
        
        % S 
        Q07 = Extinction_Prob_n_Drugs(PD1,u(1,1),c_total(j),3);
        Q08 = Extinction_Prob_n_Drugs(PD2,u(1,1),c_total(j),3);
        Q09 = Extinction_Prob_n_Drugs(PD3,u(1,1),c_total(j),3);
        
        Mono.PD1{1,i}(1,j) = Q01^(N0);
        Mono.PD2{1,i}(1,j) = Q02^(N0);
        Mono.PD3{1,i}(1,j) = Q03^(N0);
        
        Mono.PD1{2,i}(1,j) = Q04^(N0);
        Mono.PD2{2,i}(1,j) = Q05^(N0);
        Mono.PD3{2,i}(1,j) = Q06^(N0);
        
        Mono.PD1{3,i}(1,j) = Q07^(N0);
        Mono.PD2{3,i}(1,j) = Q08^(N0);
        Mono.PD3{3,i}(1,j) = Q09^(N0);
        
        
        % 2 - drug Combination: Bliss
        Q01 = Extinction_Prob_n_Drugs([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        Q02 = Extinction_Prob_n_Drugs([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        Q03 = Extinction_Prob_n_Drugs([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        
        Q04 = Extinction_Prob_n_Drugs([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        Q05 = Extinction_Prob_n_Drugs([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        Q06 = Extinction_Prob_n_Drugs([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        
        Q07 = Extinction_Prob_n_Drugs([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        Q08 = Extinction_Prob_n_Drugs([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        Q09 = Extinction_Prob_n_Drugs([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        
        Combi_B.PD1{1,i}(1,j) = Q01^(N0);
        Combi_B.PD2{1,i}(1,j) = Q02^(N0);
        Combi_B.PD3{1,i}(1,j) = Q03^(N0);
        
        Combi_B.PD1{2,i}(1,j) = Q04^(N0);
        Combi_B.PD2{2,i}(1,j) = Q05^(N0);
        Combi_B.PD3{2,i}(1,j) = Q06^(N0);
        
        Combi_B.PD1{3,i}(1,j) = Q07^(N0);
        Combi_B.PD2{3,i}(1,j) = Q08^(N0);
        Combi_B.PD3{3,i}(1,j) = Q09^(N0);
        
        % 2 - drug Combination: Bliss
        Q01 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        Q02 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        Q03 = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        
        Q04 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        Q05 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        Q06 = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        
        Q07 = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        Q08 = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        Q09 = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        
        Combi_L.PD1{1,i}(1,j) = Q01^(N0);
        Combi_L.PD2{1,i}(1,j) = Q02^(N0);
        Combi_L.PD3{1,i}(1,j) = Q03^(N0);
        
        Combi_L.PD1{2,i}(1,j) = Q04^(N0);
        Combi_L.PD2{2,i}(1,j) = Q05^(N0);
        Combi_L.PD3{2,i}(1,j) = Q06^(N0);
        
        Combi_L.PD1{3,i}(1,j) = Q07^(N0);
        Combi_L.PD2{3,i}(1,j) = Q08^(N0);
        Combi_L.PD3{3,i}(1,j) = Q09^(N0);
        
        
        % Approximations: 
        [p_est1,N_M1] = Extinction_Approx_n_Drugs(PD1,u,c_total(j),1,N0);
        [p_est2,N_M2] = Extinction_Approx_n_Drugs(PD2,u,c_total(j),1,N0);
        [p_est3,N_M3] = Extinction_Approx_n_Drugs(PD3,u,c_total(j),1,N0);
        
        [p_est4,N_M4] = Extinction_Approx_n_Drugs(PD1,u,c_total(j),2,N0);
        [p_est5,N_M5] = Extinction_Approx_n_Drugs(PD2,u,c_total(j),2,N0);
        [p_est6,N_M6] = Extinction_Approx_n_Drugs(PD3,u,c_total(j),2,N0);
     
        [p_est7,N_M7] = Extinction_Approx_n_Drugs(PD1,u,c_total(j),3,N0);
        [p_est8,N_M8] = Extinction_Approx_n_Drugs(PD2,u,c_total(j),3,N0);
        [p_est9,N_M9] = Extinction_Approx_n_Drugs(PD3,u,c_total(j),3,N0);
        
         p_est_Mono.PD1{1,i}(:,j) = p_est1';
         p_est_Mono.PD2{1,i}(:,j) = p_est2';
         p_est_Mono.PD3{1,i}(:,j) = p_est3';
         
         p_est_Mono.PD1{2,i}(:,j) = p_est4';
         p_est_Mono.PD2{2,i}(:,j) = p_est5';
         p_est_Mono.PD3{2,i}(:,j) = p_est6';
         
         p_est_Mono.PD1{3,i}(:,j) = p_est7';
         p_est_Mono.PD2{3,i}(:,j) = p_est8';
         p_est_Mono.PD3{3,i}(:,j) = p_est9';
         
         N_M_Mono.PD1{1,i}(:,j) = N_M1';
         N_M_Mono.PD2{1,i}(:,j) = N_M2';
         N_M_Mono.PD3{1,i}(:,j) = N_M3';
         
         N_M_Mono.PD1{2,i}(:,j) = N_M4';
         N_M_Mono.PD2{2,i}(:,j) = N_M5';
         N_M_Mono.PD3{2,i}(:,j) = N_M6';
         
         N_M_Mono.PD1{3,i}(:,j) = N_M7';
         N_M_Mono.PD2{3,i}(:,j) = N_M8';
         N_M_Mono.PD3{3,i}(:,j) = N_M9';
         
         
         % Approximations: Combi
        [p_est1,N_M1] = Extinction_Approx_n_Drugs([PD1,PD1],u,c_total(j)/2*ones(1,2),1*ones(1,2),N0);
        [p_est2,N_M2] = Extinction_Approx_n_Drugs([PD2,PD2],u,c_total(j)/2*ones(1,2),1*ones(1,2),N0);
        [p_est3,N_M3] = Extinction_Approx_n_Drugs([PD3,PD3],u,c_total(j)/2*ones(1,2),1*ones(1,2),N0);
        
        [p_est4,N_M4] = Extinction_Approx_n_Drugs([PD1,PD1],u,c_total(j)/2*ones(1,2),2*ones(1,2),N0);
        [p_est5,N_M5] = Extinction_Approx_n_Drugs([PD2,PD2],u,c_total(j)/2*ones(1,2),2*ones(1,2),N0);
        [p_est6,N_M6] = Extinction_Approx_n_Drugs([PD3,PD3],u,c_total(j)/2*ones(1,2),2*ones(1,2),N0);
     
        [p_est7,N_M7] = Extinction_Approx_n_Drugs([PD1,PD1],u,c_total(j)/2*ones(1,2),3*ones(1,2),N0);
        [p_est8,N_M8] = Extinction_Approx_n_Drugs([PD2,PD2],u,c_total(j)/2*ones(1,2),3*ones(1,2),N0);
        [p_est9,N_M9] = Extinction_Approx_n_Drugs([PD3,PD3],u,c_total(j)/2*ones(1,2),3*ones(1,2),N0);
        
         p_est_Combi_B.PD1{1,i}(:,j) = p_est1';
         p_est_Combi_B.PD2{1,i}(:,j) = p_est2';
         p_est_Combi_B.PD3{1,i}(:,j) = p_est3';
         
         p_est_Combi_B.PD1{2,i}(:,j) = p_est4';
         p_est_Combi_B.PD2{2,i}(:,j) = p_est5';
         p_est_Combi_B.PD3{2,i}(:,j) = p_est6';
         
         p_est_Combi_B.PD1{3,i}(:,j) = p_est7';
         p_est_Combi_B.PD2{3,i}(:,j) = p_est8';
         p_est_Combi_B.PD3{3,i}(:,j) = p_est9';
         
         N_M_Combi_B.PD1{1,i}(:,j) = N_M1';
         N_M_Combi_B.PD2{1,i}(:,j) = N_M2';
         N_M_Combi_B.PD3{1,i}(:,j) = N_M3';
         
         N_M_Combi_B.PD1{2,i}(:,j) = N_M4';
         N_M_Combi_B.PD2{2,i}(:,j) = N_M5';
         N_M_Combi_B.PD3{2,i}(:,j) = N_M6';
         
         N_M_Combi_B.PD1{3,i}(:,j) = N_M7';
         N_M_Combi_B.PD2{3,i}(:,j) = N_M8';
         N_M_Combi_B.PD3{3,i}(:,j) = N_M9';
         
        % Loewe  
        [p_est1,N_M1] = Extinction_Approx_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)/2*ones(1,2),1*ones(1,2),N0);
        [p_est2,N_M2] = Extinction_Approx_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)/2*ones(1,2),1*ones(1,2),N0);
        [p_est3,N_M3] = Extinction_Approx_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)/2*ones(1,2),1*ones(1,2),N0);
        
        [p_est4,N_M4] = Extinction_Approx_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)/2*ones(1,2),2*ones(1,2),N0);
        [p_est5,N_M5] = Extinction_Approx_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)/2*ones(1,2),2*ones(1,2),N0);
        [p_est6,N_M6] = Extinction_Approx_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)/2*ones(1,2),2*ones(1,2),N0);
     
        [p_est7,N_M7] = Extinction_Approx_n_Drugs_LOEWE([PD1,PD1],u,c_total(j)/2*ones(1,2),3*ones(1,2),N0);
        [p_est8,N_M8] = Extinction_Approx_n_Drugs_LOEWE([PD2,PD2],u,c_total(j)/2*ones(1,2),3*ones(1,2),N0);
        [p_est9,N_M9] = Extinction_Approx_n_Drugs_LOEWE([PD3,PD3],u,c_total(j)/2*ones(1,2),3*ones(1,2),N0);
        
         p_est_Combi_L.PD1{1,i}(:,j) = p_est1';
         p_est_Combi_L.PD2{1,i}(:,j) = p_est2';
         p_est_Combi_L.PD3{1,i}(:,j) = p_est3';
         
         p_est_Combi_L.PD1{2,i}(:,j) = p_est4';
         p_est_Combi_L.PD2{2,i}(:,j) = p_est5';
         p_est_Combi_L.PD3{2,i}(:,j) = p_est6';
         
         p_est_Combi_L.PD1{3,i}(:,j) = p_est7';
         p_est_Combi_L.PD2{3,i}(:,j) = p_est8';
         p_est_Combi_L.PD3{3,i}(:,j) = p_est9';
         
         N_M_Combi_L.PD1{1,i}(:,j) = N_M1';
         N_M_Combi_L.PD2{1,i}(:,j) = N_M2';
         N_M_Combi_L.PD3{1,i}(:,j) = N_M3';
         
         N_M_Combi_L.PD1{2,i}(:,j) = N_M4';
         N_M_Combi_L.PD2{2,i}(:,j) = N_M5';
         N_M_Combi_L.PD3{2,i}(:,j) = N_M6';
         
         N_M_Combi_L.PD1{3,i}(:,j) = N_M7';
         N_M_Combi_L.PD2{3,i}(:,j) = N_M8';
         N_M_Combi_L.PD3{3,i}(:,j) = N_M9';
         
         % SGV:
         
          [~,Q01v] = Extinction_Prob_n_Drugs(PD1,u(1,1),c_total(j),1);
        [~,Q02v] = Extinction_Prob_n_Drugs(PD2,u(1,1),c_total(j),1);
        [~,Q03v] = Extinction_Prob_n_Drugs(PD3,u(1,1),c_total(j),1);
        % CR
        [~,Q04v] = Extinction_Prob_n_Drugs(PD1,u(1,1),c_total(j),2);
        [~,Q05v] = Extinction_Prob_n_Drugs(PD2,u(1,1),c_total(j),2);
        [~,Q06v]= Extinction_Prob_n_Drugs(PD3,u(1,1),c_total(j),2);
        
        % S 
        [~,Q07v] = Extinction_Prob_n_Drugs(PD1,u(1,1),c_total(j),3);
        [~,Q08v] = Extinction_Prob_n_Drugs(PD2,u(1,1),c_total(j),3);
        [~,Q09v] = Extinction_Prob_n_Drugs(PD3,u(1,1),c_total(j),3);
        
        N0_vec = N0_struct{1};
        Mono_sgv.PD1{1,i}(1,j) = prod(Q01v.^(N0_vec(i,:)));
        Mono_sgv.PD2{1,i}(1,j) = prod(Q02v.^(N0_vec(i,:)));
        Mono_sgv.PD3{1,i}(1,j) = prod(Q03v.^(N0_vec(i,:)));
        
        Mono_sgv.PD1{2,i}(1,j) = prod(Q04v.^(N0_vec(i,:)));
        Mono_sgv.PD2{2,i}(1,j) = prod(Q05v.^(N0_vec(i,:)));
        Mono_sgv.PD3{2,i}(1,j) = prod(Q06v.^(N0_vec(i,:)));
        
        Mono_sgv.PD1{3,i}(1,j) = prod(Q07v.^(N0_vec(i,:)));
        Mono_sgv.PD2{3,i}(1,j) = prod(Q08v.^(N0_vec(i,:)));
        Mono_sgv.PD3{3,i}(1,j) = prod(Q09v.^(N0_vec(i,:)));
        
        
        % 2 - drug Combination: Bliss
        [~,Q01v]= Extinction_Prob_n_Drugs([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        [~,Q02v]= Extinction_Prob_n_Drugs([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        [~,Q03v] = Extinction_Prob_n_Drugs([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        
        [~,Q04v] = Extinction_Prob_n_Drugs([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        [~,Q05v]= Extinction_Prob_n_Drugs([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        [~,Q06v] = Extinction_Prob_n_Drugs([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        
        [~,Q07v] = Extinction_Prob_n_Drugs([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        [~,Q08v] = Extinction_Prob_n_Drugs([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        [~,Q09v]= Extinction_Prob_n_Drugs([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        
        N0_vec = N0_struct{2};
        Combi_B_sgv.PD1{1,i}(1,j) = prod(Q01v.^(N0_vec(i,:)));
        Combi_B_sgv.PD2{1,i}(1,j) = prod(Q02v.^(N0_vec(i,:)));
        Combi_B_sgv.PD3{1,i}(1,j) = prod(Q03v.^(N0_vec(i,:)));
        
        Combi_B_sgv.PD1{2,i}(1,j) = prod(Q04v.^(N0_vec(i,:)));
        Combi_B_sgv.PD2{2,i}(1,j) = prod(Q05v.^(N0_vec(i,:)));
        Combi_B_sgv.PD3{2,i}(1,j) = prod(Q06v.^(N0_vec(i,:)));
        
        Combi_B_sgv.PD1{3,i}(1,j) = prod(Q07v.^(N0_vec(i,:)));
        Combi_B_sgv.PD2{3,i}(1,j) = prod(Q08v.^(N0_vec(i,:)));
        Combi_B_sgv.PD3{3,i}(1,j) = prod(Q09v.^(N0_vec(i,:)));
        
        % 2 - drug Combination: Bliss
        [~,Q01v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        [~,Q02v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        [~,Q03v] = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[1,1]);
        
        [~,Q04v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        [~,Q05v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        [~,Q06v] = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[2,2]);
        
        [~,Q07v] = Extinction_Prob_n_Drugs_LOEWE([PD1,PD1],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        [~,Q08v] = Extinction_Prob_n_Drugs_LOEWE([PD2,PD2],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        [~,Q09v] = Extinction_Prob_n_Drugs_LOEWE([PD3,PD3],u,[c_total(j)/2,c_total(j)/2],[3,3]);
        
        Combi_L_sgv.PD1{1,i}(1,j) = prod(Q01v.^(N0_vec(i,:)));
        Combi_L_sgv.PD2{1,i}(1,j) = prod(Q02v.^(N0_vec(i,:)));
        Combi_L_sgv.PD3{1,i}(1,j) = prod(Q03v.^(N0_vec(i,:)));
        
        Combi_L_sgv.PD1{2,i}(1,j) = prod(Q04v.^(N0_vec(i,:)));
        Combi_L_sgv.PD2{2,i}(1,j) = prod(Q05v.^(N0_vec(i,:)));
        Combi_L_sgv.PD3{2,i}(1,j) = prod(Q06v.^(N0_vec(i,:)));
        
        Combi_L_sgv.PD1{3,i}(1,j) = prod(Q07v.^(N0_vec(i,:)));
        Combi_L_sgv.PD2{3,i}(1,j) = prod(Q08v.^(N0_vec(i,:)));
        Combi_L_sgv.PD3{3,i}(1,j) = prod(Q09v.^(N0_vec(i,:)));
end
end 

%% 4. Plot Data - Figure 3
pos = [-0.2,1.15]; 

fig1=figure(1); 
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,15.5,14]; 
T = tiledlayout(3,3);  

for i = 1:3 
    nexttile
    plots(3)=plot(1+c_total,Mono.PD3{1,i},'Color',col1);
    hold on 
    plots(1)=plot(1+c_total,Combi_B.PD3{1,i},'Color',col2);
    hold on 
    plots(2)=plot(1+c_total,Combi_L.PD3{1,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    if i == 1
        leg3=legend(plots,'n=2: \textsf{Bliss}','n=2: \textsf{Loewe}','n=1','Location','southeast','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    str = strcat(strs_u{i},',','\ $\kappa_i = 0.5$');
    title(str,'FontSize',font1); 
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono.PD2{1,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B.PD2{1,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L.PD2{1,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
     str = strcat(strs_u{i},',','\ $\kappa_i = 2$');
    title(str,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{3+i},'Units','normalized','FontSize',font3)
end

% Special cases: 
nexttile
plot(1+c_total,Mono_sgv.PD2{1,1},'Color',col1);
hold on 
plot(1+c_total,Combi_B_sgv.PD2{1,1},'Color',col2);
hold on 
plot(1+c_total,Combi_L_sgv.PD2{1,1},'Color',col3);
hold on 
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{7},'Units','normalized','FontSize',font3)
title({'\textsf{Bactericidal (CK)}';'$N_{M_i}=1$'},'FontSize',font1); 


nexttile
plot(1+c_total,Mono.PD2{2,1},'Color',col1);
hold on 
plot(1+c_total,Combi_B.PD2{2,1},'Color',col2);
hold on 
plot(1+c_total,Combi_L.PD2{2,1},'Color',col3);
hold on 
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{8},'Units','normalized','FontSize',font3)
title({'\textsf{Additional results for} $u_i=10^{-11}$ \textsf{and} $\kappa_i = 2$';'\textsf{Bactericidal (CR)}';'$N_{M_i} = 0$'},'FontSize',font1); 

nexttile
plot(1+c_total,Mono.PD2{3,1},'Color',col1);
hold on 
plot(1+c_total,Combi_B.PD2{3,1},'Color',col2);
hold on 
plot(1+c_total,Combi_L.PD2{3,1},'Color',col3);
hold on 
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{9},'Units','normalized','FontSize',font3)
title({'\textsf{Bacteriostatic (S)}';'$N_{M_i} = 0$'},'FontSize',font1); 


title(T,'\textsf{Bactericidal (CK),} $N_{M_i}=0$','FontSize',font2,'Interpreter','latex'); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,{'\textsf{Probability of treatment success} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% ========================================================================
% SI FIGURES 
%==========================================================================

%% Plot Data - Figure S14
pos = [-0.2,1.15];
j = 1; 

% Approximation: 
fig4=figure(4); 
fig4.Units = 'centimeters'; 
fig4.Position=[10,2,15.5,13]; 
T = tiledlayout(3,3);  

nexttile 
plots1(1)=plot(c_total+1,p_est_Mono.PD3{1,j}(2,:),'Color',col1); 
hold on 
plots1(3)=plot(c_total+1,p_est_Combi_B.PD3{1,j}(2,:),'Color',col2); 
hold on
plot(c_total+1,p_est_Combi_B.PD3{1,j}(4,:),'--','Color',col2); 
hold on
plots1(2)=plot(c_total+1,p_est_Combi_L.PD3{1,j}(2,:),'Color',col3); 
hold on
plot(c_total+1,p_est_Combi_L.PD3{1,j}(4,:),'--','Color',col3); 
hold on
plots1(4) = plot(nan,nan,'Color',0.2*ones(1,4)); 
hold on
plots1(5) = plot(nan,nan,'--','Color',0.6*ones(1,3)); 
set(gca,'FontSize',font1); 
leg3=legend(plots1,'n=1','n=2 \textsf{Loewe}','n=2 \textsf{Bliss}','$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg3.ItemTokenSize = [15,18];
axis tight 
ylabel('$p_{\mathrm{est}}^{M_I}$','Interpreter','latex','FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{1},'Units','normalized','FontSize',font3)
title({'\textsf{Establishment Probability}';'$\kappa_i = 0.5$'},'FontSize',font1); 

nexttile 
plot(c_total+1,N_M_Mono.PD3{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,N_M_Combi_B.PD3{1,j}(2,:)+N_M_Combi_B.PD3{1,j}(3,:),'Color',col2); 
hold on
plot(c_total+1,N_M_Combi_B.PD3{1,j}(4,:),'--','Color',col2); 
hold on
plot(c_total+1,N_M_Combi_L.PD3{1,j}(2,:)+N_M_Combi_L.PD3{1,j}(3,:),'Color',col3); 
hold on
plot(c_total+1,N_M_Combi_L.PD3{1,j}(4,:),'--','Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'YTick',[10^-10,10^-5,10^0],'YTickLabels',{'$10^{-10}$','$10^{-5}$','1'});
set(gca,'XScale','log'); 
set(gca,'YScale','log');  
text(pos(1),pos(2),charlbl{2},'Units','normalized','FontSize',font3)
title({'\textsf{Number of de novo Mutants}';'$\kappa_i = 0.5$'},'FontSize',font1); 
ylabel('$N_{M_I}^{\mathrm{(tot)}}$','Interpreter','latex','FontSize',font1); 

nexttile 
plot(c_total+1,p_est_Mono.PD3{1,j}(2,:).*N_M_Mono.PD3{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,p_est_Combi_B.PD3{1,j}(2,:).*N_M_Combi_B.PD3{1,j}(2,:)+p_est_Combi_B.PD3{1,j}(3,:).*N_M_Combi_B.PD3{1,j}(3,:)+p_est_Combi_B.PD3{1,j}(4,:).*N_M_Combi_B.PD3{1,j}(4,:),'Color',col2); 
hold on
plot(c_total+1,p_est_Combi_L.PD3{1,j}(2,:).*N_M_Combi_L.PD3{1,j}(2,:)+p_est_Combi_L.PD3{1,j}(3,:).*N_M_Combi_L.PD3{1,j}(3,:)+p_est_Combi_L.PD3{1,j}(4,:).*N_M_Combi_L.PD3{1,j}(4,:),'Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log');  
text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3)
set(gca,'YTick',[10^-10,10^-5,10^0],'YTickLabels',{'$10^{-10}$','$10^{-5}$','1'});
title({'\textsf{Number of successful Mutants} ';'$\kappa_i = 0.1$'},'FontSize',font1); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 


nexttile 
plot(c_total+1,p_est_Mono.PD1{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,p_est_Combi_B.PD1{1,j}(2,:),'Color',col2); 
hold on
plot(c_total+1,p_est_Combi_B.PD1{1,j}(4,:),'--','Color',col2); 
hold on
plot(c_total+1,p_est_Combi_L.PD1{1,j}(2,:),'Color',col3); 
hold on
plot(c_total+1,p_est_Combi_L.PD1{1,j}(4,:),'--','Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
% ylabel('\textsf{Establishment probability}  ($p_{\mathrm{est}}^{M_I}$)','Interpreter','latex','FontSize',font2); 
ylabel('$p_{\mathrm{est}}^{M_I}$','Interpreter','latex','FontSize',font1); 
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3)
title('$\kappa_i = 1$','FontSize',font1); 

nexttile 
plot(c_total+1,N_M_Mono.PD1{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,N_M_Combi_B.PD1{1,j}(2,:)+N_M_Combi_B.PD1{1,j}(3,:),'Color',col2); 
hold on
plot(c_total+1,N_M_Combi_B.PD1{1,j}(4,:),'--','Color',col2); 
hold on
plot(c_total+1,N_M_Combi_L.PD1{1,j}(2,:)+N_M_Combi_L.PD1{1,j}(3,:),'Color',col3); 
hold on
plot(c_total+1,N_M_Combi_L.PD1{1,j}(4,:),'--','Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylabel('$N_{M_I}^{\mathrm{(tot)}}$','Interpreter','latex','FontSize',font1); 
set(gca,'YTick',[10^-10,10^-5,10^0],'YTickLabels',{'$10^{-10}$','$10^{-5}$','1'});
text(pos(1),pos(2),charlbl{5},'Units','normalized','FontSize',font3)
title('$\kappa_i = 1$','FontSize',font1); 

nexttile 
plot(c_total+1,p_est_Mono.PD1{1,j}(2,:).*N_M_Mono.PD1{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,p_est_Combi_B.PD1{1,j}(2,:).*N_M_Combi_B.PD1{1,j}(2,:)+p_est_Combi_B.PD1{1,j}(3,:).*N_M_Combi_B.PD1{1,j}(3,:)+p_est_Combi_B.PD1{1,j}(4,:).*N_M_Combi_B.PD1{1,j}(4,:),'Color',col2); 
hold on
plot(c_total+1,p_est_Combi_L.PD1{1,j}(2,:).*N_M_Combi_L.PD1{1,j}(2,:)+p_est_Combi_L.PD1{1,j}(3,:).*N_M_Combi_L.PD1{1,j}(3,:)+p_est_Combi_L.PD1{1,j}(4,:).*N_M_Combi_L.PD1{1,j}(4,:),'Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 
set(gca,'YTick',[10^-10,10^-5,10^0],'YTickLabels',{'$10^{-10}$','$10^{-5}$','1'});
text(pos(1),pos(2),charlbl{6},'Units','normalized','FontSize',font3)
title('$\kappa_i = 1$','FontSize',font1); 

nexttile 
plot(c_total+1,p_est_Mono.PD2{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,p_est_Combi_B.PD2{1,j}(2,:),'Color',col2); 
hold on
plot(c_total+1,p_est_Combi_B.PD2{1,j}(4,:),'--','Color',col2); 
hold on
plot(c_total+1,p_est_Combi_L.PD2{1,j}(2,:),'Color',col3); 
hold on
plot(c_total+1,p_est_Combi_L.PD2{1,j}(4,:),'--','Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
text(pos(1),pos(2),charlbl{7},'Units','normalized','FontSize',font3)
title('$\kappa_i = 2$','FontSize',font1); 
ylabel('$p_{\mathrm{est}}^{M_I}$','Interpreter','latex','FontSize',font1); 

nexttile 
plot(c_total+1,N_M_Mono.PD2{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,N_M_Combi_B.PD2{1,j}(2,:)+N_M_Combi_B.PD2{1,j}(3,:),'Color',col2); 
hold on
plot(c_total+1,N_M_Combi_B.PD2{1,j}(4,:),'--','Color',col2); 
hold on
plot(c_total+1,N_M_Combi_L.PD2{1,j}(2,:)+N_M_Combi_L.PD2{1,j}(3,:),'Color',col3); 
hold on
plot(c_total+1,N_M_Combi_L.PD2{1,j}(4,:),'--','Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'YTick',[10^-10,10^-5,10^0],'YTickLabels',{'$10^{-10}$','$10^{-5}$','1'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
text(pos(1),pos(2),charlbl{8},'Units','normalized','FontSize',font3)
title('$\kappa_i = 2$','FontSize',font1); 
ylabel('$N_{M_I}^{\mathrm{(tot)}}$','Interpreter','latex','FontSize',font1); 

nexttile 
plot(c_total+1,p_est_Mono.PD2{1,j}(2,:).*N_M_Mono.PD2{1,j}(2,:),'Color',col1); 
hold on 
plot(c_total+1,p_est_Combi_B.PD2{1,j}(2,:).*N_M_Combi_B.PD2{1,j}(2,:)+p_est_Combi_B.PD2{1,j}(3,:).*N_M_Combi_B.PD2{1,j}(3,:)+p_est_Combi_B.PD2{1,j}(4,:).*N_M_Combi_B.PD2{1,j}(4,:),'Color',col2); 
hold on
plot(c_total+1,p_est_Combi_L.PD2{1,j}(2,:).*N_M_Combi_L.PD2{1,j}(2,:)+p_est_Combi_L.PD2{1,j}(3,:).*N_M_Combi_L.PD2{1,j}(3,:)+p_est_Combi_L.PD2{1,j}(4,:).*N_M_Combi_L.PD2{1,j}(4,:),'Color',col3); 
hold on
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
set(gca,'YTick',[10^-10,10^-5,10^0],'YTickLabels',{'$10^{-10}$','$10^{-5}$','1'});
text(pos(1),pos(2),charlbl{9},'Units','normalized','FontSize',font3)
title('$\kappa_i = 2$','FontSize',font1); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 

    
title(T,'\textsf{Components of the approximation (bactericidal (CK), $u_i = 10^{-9}$) }','Interpreter','latex','FontSize',font2); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
set(findall(fig4,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig4,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig4,'-property','XLim'),'XLim',[1,41]); 


%% Plot data - Figure S10

pos = [-0.2,1.2]; 
fig1=figure(1); 
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,15.5,5]; 
T = tiledlayout(1,3);  

for i = 1:3 
    nexttile
    plots(3)=plot(1+c_total,Mono.PD1{1,i},'Color',col1);
    hold on 
    plots(1)= plot(1+c_total,Combi_B.PD1{1,i},'Color',col2);
    hold on 
    plots(2)=plot(1+c_total,Combi_L.PD1{1,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    if i == 3
        leg3=legend(plots,'n=2: \textsf{Bliss}','n=2: \textsf{Loewe}','n=1','Location','northwest','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$');
    title(str,'FontSize',font1); 
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)
end


title(T,'\textsf{Bactericidal (CK),} $N_{0,M_I}=0$','Interpreter','latex','FontSize',font2); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,{'\textsf{Probability of treatment}'; '\textsf{success} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');




%% Plot Data - Figure S11

pos = [-0.2,1.2]; 
fig1=figure(1); 
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,15.5,13]; 
T = tiledlayout(3,3);  

for i = 1:3 
    nexttile
    plots(3)=plot(1+c_total,Mono_sgv.PD3{1,i},'Color',col1);
    hold on 
    plots(1)= plot(1+c_total,Combi_B_sgv.PD3{1,i},'Color',col2);
    hold on 
    plots(2)=plot(1+c_total,Combi_L_sgv.PD3{1,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    if i == 3
        leg3=legend(plots,'n=2: \textsf{Bliss}','n=2: \textsf{Loewe}','n=1','Location','northwest','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    str = strcat(strs_u{i},',','$\ \kappa_i = 0.5$');
    title(str,'FontSize',font1); 
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono_sgv.PD1{1,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B_sgv.PD1{1,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L_sgv.PD1{1,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{3+i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$');
    title(str,'FontSize',font1); 
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono_sgv.PD2{1,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B_sgv.PD2{1,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L_sgv.PD2{1,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{6+i},'Units','normalized','FontSize',font3)
 str = strcat(strs_u{i},',','$\ \kappa_i = 2$');
    title(str,'FontSize',font1); 
end

title(T,'\textsf{Bactericidal (CK), presence of pre-existing mutants} ($N_{0,M_I}>0$)','Interpreter','latex','FontSize',font2); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,{'\textsf{Probability of treatment success} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');


%% %% Plot Data - Figure S12

pos = [-0.15,1.15]; 

fig2=figure(2); 
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,13]; 
T = tiledlayout(3,3);  

for i = 1:3 
    nexttile
    plots(3) = plot(1+c_total,Mono.PD3{2,i},'Color',col1);
    hold on 
    plots(1) = plot(1+c_total,Combi_B.PD3{2,i},'Color',col2);
    hold on 
    plots(2) = plot(1+c_total,Combi_L.PD3{2,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    if i == 1
        leg3=legend(plots,'n=2: \textsf{Bliss}','n=2: \textsf{Loewe}','n=1','Location','southeast','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 0.5$');
    title(str,'FontSize',font1); 
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono.PD1{2,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B.PD1{2,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L.PD1{2,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{3+i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$');
    title(str,'FontSize',font1); 
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono.PD2{2,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B.PD2{2,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L.PD2{2,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{6+i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 2$');
    title(str,'FontSize',font1); 
end

title(T,'\textsf{Bactericidal (CR),} $N_{0,M_I}=0$','Interpreter','latex','FontSize',font2); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,{'\textsf{Probability of treatment success} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');

%% Plot Data - Figure S13
pos = [-0.15,1.15]; 

fig3=figure(3); 
fig3.Units = 'centimeters'; 
fig3.Position=[10,2,15.5,13]; 
T = tiledlayout(3,3);  

for i = 1:3 
    nexttile
    plots(3)=plot(1+c_total,Mono.PD3{3,i},'Color',col1);
    hold on 
    plots(1)=plot(1+c_total,Combi_B.PD3{3,i},'Color',col2);
    hold on 
    plots(2)=plot(1+c_total,Combi_L.PD3{3,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    if i == 1
        leg3=legend(plots,'n=2: \textsf{Bliss}','n=2: \textsf{Loewe}','n=1','Location','southeast','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 0.5$');
    title(str,'FontSize',font1); 
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono.PD1{3,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B.PD1{3,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L.PD1{3,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{3+i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 1$');
    title(str,'FontSize',font1); 
end

for i = 1:3 
    nexttile
    plot(1+c_total,Mono.PD2{3,i},'Color',col1);
    hold on 
    plot(1+c_total,Combi_B.PD2{3,i},'Color',col2);
    hold on 
    plot(1+c_total,Combi_L.PD2{3,i},'Color',col3);
    hold on 
    set(gca,'FontSize',font1); 
    axis tight 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    set(gca,'XScale','log'); 
    text(pos(1),pos(2),charlbl{6+i},'Units','normalized','FontSize',font3)
    str = strcat(strs_u{i},',','$\ \kappa_i = 2$');
    title(str,'FontSize',font1); 
end

title(T,'\textsf{Bacteriostatic (S),} $N_{0,M_I}=0$','Interpreter','latex','FontSize',font2); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,{'\textsf{Probability of treatment success} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
%% ========================================================================
% PD Functions (Figures S4-S6)
% =========================================================================
%% Plot PD curves for the Mutants 
% For growth rates of different types 
PD2res = PD2; 
PD2res.zMIC = PD2.zMIC*10;
PD1res = PD1; 
PD1res.zMIC = PD1.zMIC*10;
PD3res = PD3; 
PD3res.zMIC = PD3.zMIC*10;
%% Plot Data 
pos = [-0.15,1.075];
n = 2; 

for i = 1:3 
fig4=figure(4+i);
T=tiledlayout(3,3);
T.TileSpacing = 'normal';
fig4.Units = 'centimeters'; 
fig4.Position=[10,1,15.5,14];   
mode = i*ones(1,n);

% BLISS - kappa = 1
PD_j{1} = repmat(PD3,[1 n]); 
PD_j{2} = [PD3res,repmat(PD3,[1 n-1])]; 
PD_j{3} = [PD3res,PD3res,repmat(PD3,[1 n-2])]; 

for j = 1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
plots_m(1) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,[c,0],mode),c_total),'Color',col1);
hold on 
plots_m(3) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,c/2*ones(1,2),mode),c_total),'Color',col2);
hold on 
plots_m(2) = plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD_temp,c/2*ones(1,2),mode),c_total),'Color',col3);
set(gca,'FontSize',font1);
set(gca,'XTickLabels',[]);
if i > 2
    ylim([-0.15,0.7]);
else
ylim([-1,0.7]);
end
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});

text(pos(1),pos(2),charlbl{j},'Units','normalized','FontSize',font3)
if j == 1
    title({'\textsf{Wildtype}';'$\kappa_i = 0.5$'},'FontSize',font1); 
    leg3=legend(plots_m,'n=1','n=2: \textsf{Loewe}','n=2: \textsf{Bliss}','Location','northeast','Interpreter','latex');
    leg3.ItemTokenSize = [15,18];
    end
if j ==2 
    title({'\textsf{Single mutant}';'$\kappa_i = 0.5$'},'FontSize',font1);
end
if j == 3
    title({'\textsf{Double mutant}';'$\kappa_i = 0.5$'},'FontSize',font1);
    
end
end

% BLISS - kappa = 1
PD_j{1} = repmat(PD1,[1 n]); 
PD_j{2} = [PD1res,repmat(PD1,[1 n-1])]; 
PD_j{3} = [PD1res,PD1res,repmat(PD1,[1 n-2])]; 

for j = 1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
plots_m(1) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,[c,0],mode),c_total),'Color',col1);
hold on 
plots_m(3) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,c/2*ones(1,2),mode),c_total),'Color',col2);
hold on 
plots_m(2) = plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD_temp,c/2*ones(1,2),mode),c_total),'Color',col3);
set(gca,'FontSize',font1);
set(gca,'XTickLabels',[]);
if i > 2
    ylim([-0.15,0.7]);
else
ylim([-1,0.7]);
end
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});

text(pos(1),pos(2),charlbl{j+3},'Units','normalized','FontSize',font3)
title('$\kappa_i = 1$','FontSize',font1); 
end


% BLISS - kappa = 2
PD_j{1} = repmat(PD2,[1 n]); 
PD_j{2} = [PD2res,repmat(PD2,[1 n-1])]; 
PD_j{3} = [PD2res,PD2res,repmat(PD2,[1 n-2])]; 

for j = 1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,[c,0],mode),c_total),'Color',col1);
hold on 
plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,c/2*ones(1,2),mode),c_total),'Color',col2);
hold on 
plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD_temp,c/2*ones(1,2),mode),c_total),'Color',col3);
set(gca,'FontSize',font1);
set(gca,'FontSize',font1); 
if i > 2
    ylim([-0.15,0.7]);
else
ylim([-1,0.7]);
end
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});

text(pos(1),pos(2),charlbl{j+6},'Units','normalized','FontSize',font3)
title('$\kappa_i = 1$','FontSize',font1); 
end

if i == 1
    title(T,'\textsf{Bactericidal (CK)}','Interpreter','latex','FontSize',font2);
elseif i == 2
    title(T,'\textsf{Bactericidal (CR)}','Interpreter','latex','FontSize',font2);
else 
    title(T,'\textsf{Bacteriostatic (S)}','Interpreter','latex','FontSize',font2);
end

xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Net growth rate} $\psi$','Interpreter','latex','FontSize',font1);
set(findall(fig4,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig4,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig4,'-property','XLim'),'XLim',[0,15]);
set(findall(fig4,'-property','XSale'),'XScale','log');
end
