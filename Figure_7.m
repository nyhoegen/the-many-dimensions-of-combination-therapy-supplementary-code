% This skript produces the figure displayed in Result Section 4: 
% Compariosn of combination admonstered at different ratios
% additional figures used in the Supplement: 

% - Figure 7
% - Figures S9; S29-35
%
% Intialize Matlab:

clc 
clear 
close all

%% Disclaimer: 
% the code shows how the data of the figures can be generated
% the figures used in the manuskript were postprocessed to align panels and
% text more nicely and add differences in line types 

% !!!Generating the data takes long!!! - up until n=5 runs fairly fast 
%% 1. Plot Settings

dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
green = [150,180,130]/255; 
orange = [200,130,60]/255; 
grey = [150,150,150]/255; 
occa2 = [236,177,32]/255;
lines_col = lines(7);
purple = lines_col(4,:)+0;

% Define a color vector 
col_vec = [dark;pink;light;occa;0.2*ones(1,3)];
set(0,'DefaultLineLineWidth',1.5);  
marker_type = {'-.','-','-.','-','-.','--','-'}; 
mymap = [dark;pink;green;orange;purple;occa2;grey];
n = 7; 
mymap2 = [linspace(0.2,0.8,7)', linspace(0.2,0.8,7)',linspace(0.2,0.8,7)'];
mymap4 = [linspace(dark(1),0.8,16)',linspace(dark(2),0.8,16)',linspace(dark(3),0.8,16)'];
mymap4_1 = [linspace(dark(1),1,5)',linspace(dark(2),1,5)',linspace(dark(3),1,5)'];
mymap4_2 = [linspace(pink(1),1,6)',linspace(pink(2),1,6)',linspace(pink(3),1,6)'];
mymap4 = [mymap4_1(1:3,:);0.3,0.3,0.3;mymap4_2(3:-1:1,:)];
mymap5 = [mymap4_1(1:2,:);0.3,0.3,0.3;mymap4_2(2:-1:1,:)];
% mymap4 = [linspace(dark(1),0.8,7)',linspace(dark(2),0.8,7)',linspace(dark(3),0.8,7)'];

set(0,'DefaultLineMarkerSize',5); 

%For labeling the panels 
nIDs = 12;
alphabet = ('A':'Z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}
pos = [-0.15,1.25]; 


% Fonts
font1 = 9; 
font2 = 11;
font3 = 9; 
% then use: 
% text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
%% 2. Parameters: 

% PD Parameter (1x9 struct array) 
% First drug 
PD1.lambda_0 = 0.7;   % replication rate 
PD1.mu_0 = 0.1;        % intrinsic death rate 
PD1.psi_min = -6;     % Psi_min for the first drug 
PD1.kappa = 1;          % Kappa for the first drug 
PD1.zMIC = 1;      % zMIC for the first drug 
PD1.cost = 0.1;         % cost per mutation 
PD1.benefit = 28;       % benefit of mutation 
PD1.p_max = 0.95;        % p_max value for cdial drug action coupled with replication
PD1.sigma_max = 0.95;    % sigma_max value for static drug action 

% Second drug e.g. with different kappa value 
PD2 = PD1; 
PD2.kappa = 2; 

% Third drug e.g. with different kappa value 
PD3 = PD1; 
PD3.kappa = 0.5; 

% Gerenal parameter and vectors for figures 
u_vec = [10^-11,10^-9,10^-5];
N0 = 10^10;
strs = {'$u_i = 10^{-11}$','$u_i = 10^{-9}$','$u_i = 10^{-5}$'}; 
c_total = linspace(0,35,1000); 
c_total2 = linspace(0,50,1000); 

% For growth rates of different types 
PD1res = PD1; 
PD1res.zMIC = PD1.zMIC*1.2;
PD2res = PD2; 
PD2res.zMIC = PD2.zMIC*5;
PD3res = PD3; 
PD3res.zMIC = PD3.zMIC*5;

c_total2 = linspace(0,10*PD1.zMIC,1000); 
%% 3.1 Generate Data 1 - Set1
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = 1*ones(1,n);

tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        Q1k = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD3{k,i}(1,j) = Q3k^(N0);
        
        %Loewe 
        Q1k = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD3{k,i}(1,j) = Q3k^(N0);

    end
end
end
time=toc;

% Save Data 
Set1.Bliss_PD1 = Bliss_PD1; 
Set1.Loewe_PD1 = Loewe_PD1; 
Set1.Bliss_PD2 = Bliss_PD2; 
Set1.Loewe_PD2 = Loewe_PD2; 
Set1.Bliss_PD3 = Bliss_PD3; 
Set1.Loewe_PD3 = Loewe_PD3; 

%% 3.1 Generate Data 1 - Set1_CR
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = 2*ones(1,n);

tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        Q1k = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD3{k,i}(1,j) = Q3k^(N0);
        
        %Loewe 
        Q1k = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD3{k,i}(1,j) = Q3k^(N0);

    end
end
end
time=toc;

% Save Data 
Set1_CR.Bliss_PD1 = Bliss_PD1; 
Set1_CR.Loewe_PD1 = Loewe_PD1; 
Set1_CR.Bliss_PD2 = Bliss_PD2; 
Set1_CR.Loewe_PD2 = Loewe_PD2; 
Set1_CR.Bliss_PD3 = Bliss_PD3; 
Set1_CR.Loewe_PD3 = Loewe_PD3; 

%% 3.1 Generate Data 1 - Set1_CR
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = 2*ones(1,n);

tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        Q1k = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD3{k,i}(1,j) = Q3k^(N0);
        
        %Loewe 
        Q1k = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD3{k,i}(1,j) = Q3k^(N0);

    end
end
end
time=toc;

% Save Data 
Set1_CR.Bliss_PD1 = Bliss_PD1; 
Set1_CR.Loewe_PD1 = Loewe_PD1; 
Set1_CR.Bliss_PD2 = Bliss_PD2; 
Set1_CR.Loewe_PD2 = Loewe_PD2; 
Set1_CR.Bliss_PD3 = Bliss_PD3; 
Set1_CR.Loewe_PD3 = Loewe_PD3; 


%% 3.1 Generate Data 1 - Set1_S
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = 3*ones(1,n);

tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        Q1k = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Bliss_PD3{k,i}(1,j) = Q3k^(N0);
        
        %Loewe 
        Q1k = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD1{k,i}(1,j) = Q1k^(N0);
        
        Q2k = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD2{k,i}(1,j) = Q2k^(N0);
        
        Q3k = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),mode(1:k));
        Loewe_PD3{k,i}(1,j) = Q3k^(N0);

    end
end
end
time=toc;

% Save Data 
Set1_S.Bliss_PD1 = Bliss_PD1; 
Set1_S.Loewe_PD1 = Loewe_PD1; 
Set1_S.Bliss_PD2 = Bliss_PD2; 
Set1_S.Loewe_PD2 = Loewe_PD2; 
Set1_S.Bliss_PD3 = Bliss_PD3; 
Set1_S.Loewe_PD3 = Loewe_PD3; 


%% 4. Data with standing genetic variation

N0_struct = {}; 
N0_struct{1} = [N0-1,1;N0-100,100;N0-1000000,1000000]; 
N0_struct{2} = [N0-2,1,1,0;N0-200,100,100,0;N0-2*(999900)-100,999900,999900,100]; 
N0_struct{3} = [N0-3,ones(1,3),zeros(1,4);N0-300,100*(ones(1,3)),zeros(1,4);N0-3*(999800)-3*141,999800*ones(1,3),141*ones(1,3),0]; 
N0_struct{4} = [N0-4,ones(1,4),zeros(1,11);N0-400,100*(ones(1,4)),zeros(1,11);N0-4*(999700)-6*141,999700*ones(1,4),141*ones(1,6),zeros(1,5)];
N0_struct{5} = [N0-5,ones(1,5),zeros(1,26);N0-500,100*(ones(1,5)),zeros(1,26);N0-5*(999610)-10*141,999610*ones(1,5),141*ones(1,10),zeros(1,16)];
N0_struct{6} = [N0-6,ones(1,6),zeros(1,57);N0-600,100*(ones(1,6)),zeros(1,57);N0-6*(999510)-15*141,999510*ones(1,6),141*ones(1,15),zeros(1,42)];
N0_struct{7} = [N0-7,ones(1,7),zeros(1,120);N0-700,100*(ones(1,7)),zeros(1,120);N0-7*(999410)-15*141,999410*ones(1,7),141*ones(1,21),zeros(1,99)];

%% Set1_sgv 

n = 7;
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = ones(1,n);
m = 1; 
tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        [~,Q1k] = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD1{k,i}(1,j) = prod(Q1k.^N0_struct{k}(i,:));
        
        [~,Q2k] = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD2{k,i}(1,j) = prod(Q2k.^N0_struct{k}(i,:));
        
        [~,Q3k] = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD3{k,i}(1,j) = prod(Q3k.^N0_struct{k}(i,:));
        
        %Loewe 
        [~,Q1k] = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD1{k,i}(1,j) = prod(Q1k.^N0_struct{k}(i,:));
        
        [~,Q2k] = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD2{k,i}(1,j) = prod(Q2k.^N0_struct{k}(i,:));
        
        [~,Q3k] = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD3{k,i}(1,j) = prod(Q3k.^N0_struct{k}(i,:));

    end
end
end

% Save Data 
Set1_sgv.Bliss_PD1 = Bliss_PD1; 
Set1_sgv.Loewe_PD1 = Loewe_PD1; 
Set1_sgv.Bliss_PD2 = Bliss_PD2; 
Set1_sgv.Loewe_PD2 = Loewe_PD2; 
Set1_sgv.Bliss_PD3 = Bliss_PD3; 
Set1_sgv.Loewe_PD3 = Loewe_PD3; 


%% Set1_CR_sgv 

n = 7;
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = ones(1,n);
m = 2; 
tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        [~,Q1k] = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD1{k,i}(1,j) = prod(Q1k.^N0_struct{k}(i,:));
        
        [~,Q2k] = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD2{k,i}(1,j) = prod(Q2k.^N0_struct{k}(i,:));
        
        [~,Q3k] = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD3{k,i}(1,j) = prod(Q3k.^N0_struct{k}(i,:));
        
        %Loewe 
        [~,Q1k] = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD1{k,i}(1,j) = prod(Q1k.^N0_struct{k}(i,:));
        
        [~,Q2k] = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD2{k,i}(1,j) = prod(Q2k.^N0_struct{k}(i,:));
        
        [~,Q3k] = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD3{k,i}(1,j) = prod(Q3k.^N0_struct{k}(i,:));

    end
end
end

% Save Data 
Set1_CR_sgv.Bliss_PD1 = Bliss_PD1; 
Set1_CR_sgv.Loewe_PD1 = Loewe_PD1; 
Set1_CR_sgv.Bliss_PD2 = Bliss_PD2; 
Set1_CR_sgv.Loewe_PD2 = Loewe_PD2; 
Set1_CR_sgv.Bliss_PD3 = Bliss_PD3; 
Set1_CR_sgv.Loewe_PD3 = Loewe_PD3; 


%% Set1_S_sgv 

n = 7;
% kappa = 1
PD1n = repmat(PD1,[1,n]);
% kappa = 2
PD2n = repmat(PD2,[1,n]);
% kappa = 0.5
PD3n = repmat(PD3,[1,n]);
% Mode of Action 
mode = ones(1,n);
m = 3; 
tic
for i = 1:3 
u = u_vec(i)*ones(1,n);   
disp(i);
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
    for k = 1:n
        % Bliss 
        [~,Q1k] = Extinction_Prob_n_Drugs(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD1{k,i}(1,j) = prod(Q1k.^N0_struct{k}(i,:));
        
        [~,Q2k] = Extinction_Prob_n_Drugs(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD2{k,i}(1,j) = prod(Q2k.^N0_struct{k}(i,:));
        
        [~,Q3k] = Extinction_Prob_n_Drugs(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Bliss_PD3{k,i}(1,j) = prod(Q3k.^N0_struct{k}(i,:));
        
        %Loewe 
        [~,Q1k] = Extinction_Prob_n_Drugs_LOEWE(PD1n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD1{k,i}(1,j) = prod(Q1k.^N0_struct{k}(i,:));
        
        [~,Q2k] = Extinction_Prob_n_Drugs_LOEWE(PD2n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD2{k,i}(1,j) = prod(Q2k.^N0_struct{k}(i,:));
        
        [~,Q3k] = Extinction_Prob_n_Drugs_LOEWE(PD3n,u(1,1:k),c_total(j)/k*ones(1,k),m*ones(1,k));
        Loewe_PD3{k,i}(1,j) = prod(Q3k.^N0_struct{k}(i,:));

    end
end
end

% Save Data 
Set1_S_sgv.Bliss_PD1 = Bliss_PD1; 
Set1_S_sgv.Loewe_PD1 = Loewe_PD1; 
Set1_S_sgv.Bliss_PD2 = Bliss_PD2; 
Set1_S_sgv.Loewe_PD2 = Loewe_PD2; 
Set1_S_sgv.Bliss_PD3 = Bliss_PD3; 
Set1_S_sgv.Loewe_PD3 = Loewe_PD3; 

%% ========================================================================
% MAIN FIGURE - Figure 7
% =========================================================================
n = 7; 
pos = [-0.15,1.1]; 
fig3 = figure(3); 
fig3.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig3.Position=[10,1,15.5,20]; 
T = tiledlayout(4,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots2((n+1)-j)=plot(1+c_total2,Set1_sgv.Bliss_PD1{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots2,'n=7','n=6','n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end

axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)  
end 

for i = 1:3 
nexttile
for j = 1:n
    if j ~=4
    plot(1+c_total2,Set1_sgv.Bliss_PD2{j,i},'Color',mymap4(j,:));
    hold on 
    end
end
plot(1+c_total2,Set1_sgv.Bliss_PD2{4,i},'Color',mymap4(4,:));
hold on 
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)  
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 2 
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_sgv.Loewe_PD1{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 

end
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_sgv.Loewe_PD2{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3) 
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

T.TileSpacing = 'compact';
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bactericidal drugs (CK) combined under Bliss independence,} $N_{0,M_I}>0$'; '\textsf{Bactericidal drugs (CK) combined under Loewe additivity,} $N_{0,M_I}>0$'}','Interpreter','latex','FontSize',font2); 

set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','Ylim'),'Ylim',[0,1.1]);


%% ========================================================================
% SI FIGRES 
% =========================================================================

%% Figure S34
n = 7; 
pos = [-0.15,1.1]; 
fig3 = figure(3); 
fig3.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig3.Position=[10,1,15.5,20]; 
T = tiledlayout(4,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots2((n+1)-j)=plot(1+c_total,Set1.Bliss_PD1{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots2,'n=7','n=6','n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end

axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)  
end 

for i = 1:3 
nexttile
for j = 1:n
    if j ~=4
    plot(1+c_total,Set1.Bliss_PD2{j,i},'Color',mymap4(j,:));
    hold on 
    end
end
plot(1+c_total,Set1.Bliss_PD2{4,i},'Color',mymap4(4,:));
hold on 
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)  
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 2 
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1.Loewe_PD1{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 

end
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1.Loewe_PD2{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3) 
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

T.TileSpacing = 'compact';
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bactericidal drugs (CK) combined under Bliss independence,} $N_{0,M_I}=0$'; '\textsf{Bactericidal drugs (CK) combined under Loewe additivity,} $N_{0,M_I}=0$'}','Interpreter','latex','FontSize',font2); 

set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','Ylim'),'Ylim',[0,1.1]);
%% Figure S33

n = 7; 
pos = [-0.15,1.2]; 
fig2 = figure(2); 
fig2.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig2.Position=[10,1,15.5,11]; 
T = tiledlayout(2,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots((n+1)-j)=plot(1+c_total2,Set1_sgv.Bliss_PD3{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 0.5$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots,'n=7','n=6','n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
ylim([0,1.1]); 
end 


% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_sgv.Loewe_PD3{j,i},'Color',mymap4(j,:));
    hold on 
end
str = strcat(strs{i},',','\ $\kappa_i = 0.5$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)
ylim([0,1.1]); 
end

T.TileSpacing = 'compact';
ylabel(T,{'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bactericidal drugs (CK) combined under Bliss independence,} $N_{0,M_I}=0$'; '\textsf{Bactericidal drugs (CK) combined under Loewe additivity,} $N_{0,M_I}=0$'}','Interpreter','latex','FontSize',font2); 
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','Ylim'),'Ylim',[0,1.1]);
%% Figures S35

n = 7; 
pos = [-0.15,1.2]; 
fig2 = figure(2); 
fig2.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig2.Position=[10,1,15.5,11]; 
T = tiledlayout(2,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots((n+1)-j)=plot(1+c_total,Set1.Bliss_PD3{j,i},'Color',mymap4(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 0.5$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots,'n=7','n=6','n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
ylim([0,1.1]); 
end 


% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1.Loewe_PD3{j,i},'Color',mymap4(j,:));
    hold on 
end
str = strcat(strs{i},',','\ $\kappa_i = 0.5$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)
ylim([0,1.1]); 
end

T.TileSpacing = 'compact';
ylabel(T,{'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bactericidal drugs (CK) combined under Bliss independence,} $N_{0,M_I}=0$'; '\textsf{Bactericidal drugs (CK) combined under Loewe additivity,} $N_{0,M_I}=0$'}','Interpreter','latex','FontSize',font2); 
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','Ylim'),'Ylim',[0,1.1]);

%% Figures S31 & S32
n = 5; 
pos = [-0.15,1.1]; 
fig3 = figure(3); 
fig3.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig3.Position=[10,1,15.5,20]; 
T = tiledlayout(4,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots3((n+1)-j)=plot(1+c_total2,Set1_CR_sgv.Bliss_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots3,'n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end

axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)  
end 

for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_CR_sgv.Bliss_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)  
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 2 
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_CR_sgv.Loewe_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 

end
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_CR_sgv.Loewe_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3) 
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

T.TileSpacing = 'compact';
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bactericidal drugs (CR) combined under Bliss independence,} $N_{0,M_I}>0$'; '\textsf{Bactericidal drugs (CR) combined under Loewe additivity,} $N_{0,M_I}>0$'}','Interpreter','latex','FontSize',font2); 

set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','Ylim'),'Ylim',[0,1.1]);


fig4 = figure(4); 
fig4.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig4.Position=[10,1,15.5,20]; 
T = tiledlayout(4,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots3((n+1)-j)=plot(1+c_total2,Set1_S_sgv.Bliss_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots3,'n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end

axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)  
end 

for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_S_sgv.Bliss_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)  
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 2 
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_S_sgv.Loewe_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 

end
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total2,Set1_S_sgv.Loewe_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3) 
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

T.TileSpacing = 'compact';
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bacteriostatic drugs (S) combined under Bliss independence,} $N_{0,M_I}>0$'; '\textsf{Bacteriostatic drugs (S) combined under Loewe additivity,} $N_{0,M_I}>0$'}','Interpreter','latex','FontSize',font2); 

set(findall(fig4,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig4,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig4,'-property','Ylim'),'Ylim',[0,1.1]);
%% Figures S29 & S30
n = 5; 
pos = [-0.15,1.1]; 
fig3 = figure(3); 
fig3.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig3.Position=[10,1,15.5,20]; 
T = tiledlayout(4,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots3((n+1)-j)=plot(1+c_total,Set1_CR.Bliss_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots3,'n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end

axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)  
end 

for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1_CR.Bliss_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)  
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 2 
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1_CR.Loewe_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 

end
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1_CR.Loewe_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3) 
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

T.TileSpacing = 'compact';
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bactericidal drugs (CR) combined under Bliss independence,} $N_{0,M_I}=0$'; '\textsf{Bactericidal drugs (CR) combined under Loewe additivity,} $N_{0,M_I}=0$'}','Interpreter','latex','FontSize',font2); 

set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','Ylim'),'Ylim',[0,1.1]);


fig4 = figure(4); 
fig4.Units = 'centimeters'; 
%fig1.Position=[10,2,15.5,10]; 
fig4.Position=[10,1,15.5,20]; 
T = tiledlayout(4,3); 

% Bliss
for i = 1:3 
nexttile
for j = 1:n
    plots3((n+1)-j)=plot(1+c_total,Set1_S.Bliss_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
if i == 1
    leg1=legend(plots3,'n=5','n=4','n=3','n=2','n=1','Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18];
end

axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3)  
end 

for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1_S.Bliss_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+3},'Units','normalized','FontSize',font3)  
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 2 
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

% Loewe
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1_S.Loewe_PD1{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+6},'Units','normalized','FontSize',font3)
str = strcat(strs{i},',','\ $\kappa_i = 1$');
title(str,'FontSize',font1,'Interpreter','latex'); 

end
for i = 1:3 
nexttile
for j = 1:n
    plot(1+c_total,Set1_S.Loewe_PD2{j,i},'Color',mymap5(j,:));
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
axis tight 
set(gca,'XScale','log');
text(pos(1),pos(2),charlbl{i+9},'Units','normalized','FontSize',font3) 
str = strcat(strs{i},',','\ $\kappa_i = 2$');
title(str,'FontSize',font1,'Interpreter','latex'); 
if i == 1 
    ylabel({'\textsf{Probability of treatment sucess} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
end
end

T.TileSpacing = 'compact';
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
title(T,{'\textsf{Bacteriostatic drugs (S) combined under Bliss independence,} $N_{0,M_I}=0$'; '\textsf{Bacteriostatic drugs (S) combined under Loewe additivity,} $N_{0,M_I}=0$'}','Interpreter','latex','FontSize',font2); 

set(findall(fig4,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig4,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig4,'-property','Ylim'),'Ylim',[0,1.1]);

%% ========================================================================
% SI functions PD - Figure S9
% =========================================================================
%% 
% For growth rates of different types 
PD2res = PD2; 
PD2res.zMIC = PD2.zMIC*10;
PD1res = PD1; 
PD1res.zMIC = PD1.zMIC*10;
%% 
pos = [-0.1,1.1];
n = 5; 
fig6=figure(6);
T=tiledlayout(4,3);
T.TileSpacing = 'compact';
fig6.Units = 'centimeters'; 
fig6.Position=[10,1,15.5,20];   
mode = 1*ones(1,n);

% BLISS - kappa = 1
PD_j{1} = repmat(PD1,[1 n]); 
PD_j{2} = [PD1res,repmat(PD1,[1 n-1])]; 
PD_j{3} = [PD1res,PD1res,repmat(PD1,[1 n-2])]; 

for j = 1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap5(i,:));
    hold on 
end
set(gca,'FontSize',font1);
set(gca,'XTickLabels',[]);
ylim([-1,0.7]);
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});

text(pos(1),pos(2),charlbl{j},'Units','normalized','FontSize',font3)
if j == 1
    title({'\textsf{Wild type} ($W$)';'$\kappa_i = 1$'},'FontSize',font1); 
    end
if j ==2 
    title({'\textsf{Single mutant} ($M_{1}$)';'$\kappa_i = 1$'},'FontSize',font1);
end
if j == 3
    title({'\textsf{Double mutant} ($M_{1,2}$)';'$\kappa_i = 1$'},'FontSize',font1);
end

end


% BLISS - kappa = 2
PD_j{1} = repmat(PD2,[1 n]); 
PD_j{2} = [PD2res,repmat(PD2,[1 n-1])]; 
PD_j{3} = [PD2res,PD2res,repmat(PD2,[1 n-2])]; 

for j = 1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD_temp,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap5(i,:));
    hold on 
end
set(gca,'FontSize',font1); 
ylim([-1,0.7]);
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});

text(pos(1),pos(2),charlbl{j+3},'Units','normalized','FontSize',font3)
title('$\kappa_i = 2$','FontSize',font1);
if j == 2
    xlabel('\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 

end
if j == 1
    ylabel('\textsf{Net growth rate} $\psi$','Interpreter','latex','FontSize',font1);

end
end

% BLISS - kappa = 1
PD_j{1} = repmat(PD1,[1 n]); 
PD_j{2} = [PD1res,repmat(PD1,[1 n-1])]; 
PD_j{3} = [PD1res,PD1res,repmat(PD1,[1 n-2])]; 
for j= 1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
for i = 1:n
    plots_m(i) = plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD_temp,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap5(i,:));
    hold on 
end
set(gca,'FontSize',font1);
ylim([-1,0.7]);
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});
text(pos(1),pos(2),charlbl{j+6},'Units','normalized','FontSize',font3)

if j == 3
    leg1=legend(plots_m,{'n=1','n=2','n=3','n=4','n=5'},'Location','southeast','Interpreter','latex');
    leg1.ItemTokenSize = [15,18]; 
end
if j == 1
    title({'\textsf{Wild type} ($W$)';'$\kappa_i = 1$'},'FontSize',font1); 
    end
if j ==2 
    title({'\textsf{Single mutant} ($M_1$)';'$\kappa_i = 1$'},'FontSize',font1);
end
if j == 3
    title({'\textsf{Double mutant} ($M_{1,2}$)';'$\kappa_i = 1$'},'FontSize',font1);
end
end 

% LOEWE - kappa = 2
PD_j{1} = repmat(PD2,[1 n]); 
PD_j{2} = [PD2res,repmat(PD2,[1 n-1])]; 
PD_j{3} = [PD2res,PD2res,repmat(PD2,[1 n-2])]; 
for j=1:3
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
PD_temp = PD_j{j}; 
for k = 1:n
    if j == 2
        PD_temp(k).lambda_0 = 0.7*(1-0.1); 
    elseif j ==3 
        PD_temp(k).lambda_0 = 0.7*(1-0.1)^2; 
    end
end
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD_temp,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap5(i,:));
    hold on 
end
set(gca,'FontSize',font1);
text(pos(1),pos(2),charlbl{j+9},'Units','normalized','FontSize',font3)
ylim([-1,0.7]);
set(gca,'XTick',[1,10],'XTickLabels',{'$\mathrm{zMIC}_W$','$\beta_{\mathrm{res}}\cdot \mathrm{zMIC}_W$'});
if j == 3
    ylim([-1,0.7]);
end
title('$\kappa_i = 2$','FontSize',font1);
if j == 1
    ylabel('\textsf{Net growth rate} $\psi$','Interpreter','latex','FontSize',font1);

end 
end 

title(T,{'\textsf{Bactericidal drugs (CK) combined under Bliss independence}'; '\textsf{Bactericidal drugs (CK) combined under Loewe additivity}'}','Interpreter','latex','FontSize',font2); 
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
set(findall(fig6,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig6,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig6,'-property','XLim'),'XLim',[0,12]);
set(findall(fig6,'-property','XSale'),'XScale','log');