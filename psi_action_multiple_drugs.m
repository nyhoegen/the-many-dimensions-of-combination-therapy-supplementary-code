function [psi,lambda,mu] = psi_action_multiple_drugs(PD,c,mode)
% Function to calculate the growth rate for n_drug different drug concentrations
% and drugs with different mode of actions based on BLISS INDEPENDENCE
% 1. Bactericidal drug effect on the kill rate 
% 2. Bactericidal drug effect on the growth rate 
% 3. Bacteriostatic drug effect on the growth rate 
% 
% INPUT 
% PD       = 1xn_drug struct array discribing the PD charavteristics of the drugs
%            lambda_0 - replication rate (equal for all drugs)
%            mu_0     - intrinsic death rate (equal for all drugs) 
%            zMIC - susceptibility towards the drug 
%            kappa - steepness of the pharamcodynamic function 
%            psi_min - max kill rate of bactericidal (kill) drug
%            p_max - max drug effect of bactericidal (growth) drug 
%            sigma_max - max drug effect of bacteriostatic drug 
% c        = 1xn_drug vector of drug concnetration for all drugs
% mode     = 1xn_drug mode of action (e.g. 1-3 as above) 
% 
% OUTPUT 
% psi      = growth rate of the bacterial population 
% 
%==========================================================================
% Number of drugs used
n_drugs = length(mode);

% Initialize lambda, mu and p_vec
lambda_0 = PD(1).lambda_0; 
mu_0 = PD(1).mu_0;

lambda = lambda_0; 
mu = mu_0; 

% needed to calculate the effect of the bactericidal (growth) drug on the
% death rate 
p_vec = ones(1,n_drugs); 

% Loop over all drugs: 
 

for i = 1:n_drugs 
    % Check for each drug the mode of action and alter mu and lambda
    % accordingly 
    
    %1. Bacteriostatic (add mu(c) to mu) 
    if mode(i) == 1
        % Trow error in case that psi_min condition is not satisfied 
        if PD(i).psi_min > 0
            error('psi_min needs to be below zero')
        else
            % Calculate kill rate by antibiotic 
            mu_c = ((lambda_0-mu_0-PD(i).psi_min)*(c(i)/PD(i).zMIC).^PD(i).kappa)./((c(i)/PD(i).zMIC).^PD(i).kappa-PD(i).psi_min/(lambda_0-mu_0));
            % Add it to the overall kill rate
            mu = mu + mu_c; 
        end
    elseif mode(i) == 2
        % Due to the effect on killrate and the dependence of on the p(c) 
        % values for all bactericidal drugs the
        % increase in kill rate is calulated after the for loop 
        % - p values are saved in the vector p_vec 
        
        % Trow error in case the psi_min condition regarding p_max is not
        % fullfilled 
         cond = (lambda_0-mu_0)/(2*lambda_0);
        if PD(i).p_max <= cond 
            error('p_max needs to be above (lambda_0-mu_0)/(2*lambda_0)')
        else 
            % Calculate the inhibiton of replication by the drug
             p_c = PD(i).p_max*(c(i)/PD(i).zMIC).^PD(i).kappa./((c(i)/PD(i).zMIC).^PD(i).kappa-1+(2*PD(i).p_max*lambda_0/(lambda_0-mu_0))); 
            % Multiply it with the growth rate 
            lambda = lambda.*(1-p_c); 
            p_vec(1,i) = (1-p_c); 
        end
    elseif mode(i) == 3
        % Trow error in case the psi_min condidion rigarding sigma_max is
        % not fullfilled
        cond = (lambda_0-mu_0)/(lambda_0);
        if PD(1).sigma_max <= cond 
            error('sigma_max needs to be above (lambda_0-mu_0)/(lambda_0)')
        else 
            % Calculat factor that scales the replicatioon rate 
            sigma_c = PD(i).sigma_max*(c(i)/PD(i).zMIC).^PD(i).kappa./((c(i)/PD(i).zMIC).^PD(i).kappa+((mu_0-lambda_0*(1-PD(i).sigma_max))/(lambda_0-mu_0))); 
            % multiply value with growth rate 
            lambda = lambda.*(1-sigma_c); 

        end 
    else
        error('Mode of action needs to be a value between one and three'); 
    end
end 

% Update kill rate for the joint effect of all bacteriocidal (growth drugs)
% p_all probability of being killed by any of the drugs:
p_all = 1 - prod(p_vec); 
% add to killrate keep in mind that lambda was already multiplied with
% prod(p_vec) which need to be divided by 
mu = mu + lambda*p_all/prod(p_vec); 

% Calculate grwoth rate 
psi = lambda-mu; 

end

