% This skript produces the figure displayed in Result Section 3: 
% Compariosn of combination with different modes of action and
% additional figures used in the Supplement: 

% - Figure 4 and 5  
% - Figures S8; S15-S18
%
% Intialize Matlab:

clc 
clear 
close all
%% Disclaimer: 
% the code shows how the data of the figures can be generated
% the figures used in the manuskript were postprocessed to align panels and
% text more nicely add differences in line types 

%% 1. Plot Settings
lines_col = lines(7); 
dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
green = [150,180,130]/255; 
orange = [200,130,60]/255; 
occa2 = [236,177,32]/255;
purple = lines_col(4,:)+0.1;
col1 = dark; 
col2 = light;
col3 = pink;
grey = 0.8*ones(1,3);
% Define a color vector 
set(0,'DefaultLineLineWidth',1.5);  

%For labeling the panels 
nIDs = 9;
alphabet = ('A':'Z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}
pos = [-0.15,1.25]; 
% then use: 
% text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
% Fonts
font1 = 9; 
font2 = 11;
font3 = 9; 
%% 2. Parameters: 

% PD Parameter (1x9 struct array) 
% First drug 
PD1.lambda_0 = 0.7;   % replication rate 
PD1.mu_0 = 0.1;        % intrinsic death rate 
PD1.psi_min = -6;     % Psi_min for the first drug 
PD1.kappa = 1;          % Kappa for the first drug 
PD1.zMIC = 1;      % zMIC for the first drug 
PD1.cost = 0.1;         % cost per mutation 
PD1.benefit = 28;       % benefit of mutation 
PD1.p_max = 0.95;        % p_max value for cdial drug action coupled with replication
PD1.sigma_max = 0.95;    % sigma_max value for static drug action 

% Second drug e.g. with different kappa value 
PD2 = PD1; 
PD2.kappa = 2; 

% Third drug e.g. with different kappa value 
PD3 = PD1; 
PD3.kappa = 0.5; 

% Gerenal parameter and vectors for figures 
u_vec = [10^-11,10^-9,10^-5,0];
N0 = 10^10;
strs_u = {'$u_i = 10^{-11}$','$u_i = 10^{-9}$','$u_i = 10^{-5}$'}; 
c_total = linspace(0,35,1000); 

N0_struct = {}; 
N0_struct{1} = [N0-1,1,0,0;N0-100,100,0,0;N0-1000000,1000000,0,0]; 
N0_struct{2} = [N0-2,1,1,0;N0-200,100,100,0;N0-2*(999900)-100,999900,999900,100]; 

%% 3. Generate Data for all possible Combinations:
% Generate data for the plots 

% To generate S16: set PD_1 and PD_2 to PD2 
% To generate S17: set PD_1 and PD_2 to PD3

PD_1 = PD1;
PD_1.psi_min = PD_1.lambda_0-PD_1.mu_0-PD_1.sigma_max*PD_1.lambda_0;
PD_1.p_max = PD_1.sigma_max/2;

% For a larger bactericidal effect of the bactericidal drugs 
PD_2 = PD1;
PD_2.psi_min = PD_2.lambda_0-PD_2.mu_0-2*PD_2.p_max*PD_2.lambda_0;


for i = 1:4 
u = u_vec(i)*ones(1,2); 
if i ==4 
    disp(u)
end
% Mutation rate for each resistance mutation
for j = 1:length(c_total)
        % Mono Therapies 
        [Q01,Q01v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[1,1]);
        [Q02,Q02v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[2,2]);
        [Q03,Q03v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,[c_total(j),0],[3,3]);
        
        % Combinations of drugs with different modes of action 
        [Q04,Q04v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[2,1]);
        [Q05,Q05v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[1,3]);
        [Q06,Q06v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[2,3]);
        [Q07,Q07v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[1,1]);
        [Q08,Q08v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[2,2]);
        [Q09,Q09v] = Extinction_Prob_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[3,3]);
        
        % Larger bactericidal effect
        % Mono-therapies
        [Q010,Q010v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,[c_total(j),0],[1,1]);
        [Q011,Q011v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,[c_total(j),0],[2,2]);
        [Q012,Q012v] = Extinction_Prob_n_Drugs([PD_2,PD_1],u,c_total(j)/2*ones(1,2),[2,1]);
        [Q013,Q013v] = Extinction_Prob_n_Drugs([PD_2,PD_1],u,c_total(j)/2*ones(1,2),[1,3]);
        [Q014,Q014v] = Extinction_Prob_n_Drugs([PD_2,PD_1],u,c_total(j)/2*ones(1,2),[2,3]);
        [Q015,Q015v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)/2*ones(1,2),[1,1]);
        [Q016,Q016v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)/2*ones(1,2),[2,2]);
        [Q017,Q017v] = Extinction_Prob_n_Drugs([PD_2,PD_2],u,c_total(j)/2*ones(1,2),[1,2]);
        
        Q0_Mode{1,i}(1,j) = Q01^(N0);
        Q0_Mode{2,i}(1,j) = Q02^(N0); 
        Q0_Mode{3,i}(1,j) = Q03^(N0);
        Q0_Mode{4,i}(1,j) = Q04^(N0);
        Q0_Mode{5,i}(1,j) = Q05^(N0);
        Q0_Mode{6,i}(1,j) = Q06^(N0);
        Q0_Mode{7,i}(1,j) = Q07^(N0); 
        Q0_Mode{8,i}(1,j) = Q08^(N0);
        Q0_Mode{9,i}(1,j) = Q09^(N0); 
        
        Q0_Mode_2{1,i}(1,j) = Q010^(N0);
        Q0_Mode_2{2,i}(1,j) = Q011^(N0); 
        Q0_Mode_2{3,i}(1,j) = Q012^(N0);
        Q0_Mode_2{4,i}(1,j) = Q013^(N0);
        Q0_Mode_2{5,i}(1,j) = Q014^(N0);
        Q0_Mode_2{6,i}(1,j) = Q015^(N0);
        Q0_Mode_2{7,i}(1,j) = Q016^(N0); 
        Q0_Mode_2{8,i}(1,j) = Q017^(N0); 

        % Data for extinction Approximation
        [p_est1,N_M1] = Extinction_Approx_n_Drugs(PD_1,u,c_total(j),1,N0); 
        [p_est2,N_M2] = Extinction_Approx_n_Drugs(PD_1,u,c_total(j),2,N0); 
        [p_est3,N_M3] = Extinction_Approx_n_Drugs(PD_1,u,c_total(j),3,N0);
          
        [p_est4,N_M4] = Extinction_Approx_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[1,2],N0);
        [p_est5,N_M5] = Extinction_Approx_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[1,3],N0); 
        [p_est6,N_M6] = Extinction_Approx_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[2,3],N0);
        [p_est7,N_M7] = Extinction_Approx_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[1,1],N0);
        [p_est8,N_M8] = Extinction_Approx_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[2,2],N0);
        [p_est9,N_M9] = Extinction_Approx_n_Drugs([PD_1,PD_1],u,c_total(j)/2*ones(1,2),[3,3],N0);
        
        [p_est10,N_M10] = Extinction_Approx_n_Drugs(PD_2,u,c_total(j),1,N0); 
        [p_est11,N_M11] = Extinction_Approx_n_Drugs(PD_2,u,c_total(j),2,N0); 
        [p_est12,N_M12] = Extinction_Approx_n_Drugs([PD_2,PD_1],u,c_total(j)/2*ones(1,2),[2,1],N0);
        [p_est13,N_M13] = Extinction_Approx_n_Drugs([PD_2,PD_1],u,c_total(j)/2*ones(1,2),[1,3],N0); 
        [p_est14,N_M14] = Extinction_Approx_n_Drugs([PD_2,PD_1],u,c_total(j)/2*ones(1,2),[2,3],N0);
        [p_est15,N_M15] = Extinction_Approx_n_Drugs([PD_2,PD_2],u,c_total(j)/2*ones(1,2),[1,1],N0);
        [p_est16,N_M16] = Extinction_Approx_n_Drugs([PD_2,PD_2],u,c_total(j)/2*ones(1,2),[2,2],N0);
        [p_est17,N_M17] = Extinction_Approx_n_Drugs([PD_2,PD_2],u,c_total(j)/2*ones(1,2),[1,2],N0);
        
        p_est{1,i}(:,j) = p_est1'; 
        p_est{2,i}(:,j) = p_est2'; 
        p_est{3,i}(:,j) = p_est3'; 
        p_est{4,i}(:,j) = p_est4';        
        p_est{5,i}(:,j) = p_est5'; 
        p_est{6,i}(:,j) = p_est6'; 
        p_est{7,i}(:,j) = p_est7'; 
        p_est{8,i}(:,j) = p_est8'; 
        p_est{9,i}(:,j) = p_est9'; 

        p_est_2{1,i}(:,j) = p_est10'; 
        p_est_2{2,i}(:,j) = p_est11'; 
        p_est_2{3,i}(:,j) = p_est12'; 
        p_est_2{4,i}(:,j) = p_est13';        
        p_est_2{5,i}(:,j) = p_est14'; 
        p_est_2{6,i}(:,j) = p_est15'; 
        p_est_2{7,i}(:,j) = p_est16';
        p_est_2{8,i}(:,j) = p_est17';
        
        N_M{1,i}(:,j) = N_M1';
        N_M{2,i}(:,j) = N_M2';
        N_M{3,i}(:,j) = N_M3';
        N_M{4,i}(:,j) = N_M4';
        N_M{5,i}(:,j) = N_M5';
        N_M{6,i}(:,j) = N_M6';
        N_M{7,i}(:,j) = N_M7';
        N_M{8,i}(:,j) = N_M8';
        N_M{9,i}(:,j) = N_M9';
        
        N_M_2{1,i}(:,j) = N_M10';
        N_M_2{2,i}(:,j) = N_M11';
        N_M_2{3,i}(:,j) = N_M12';
        N_M_2{4,i}(:,j) = N_M13';
        N_M_2{5,i}(:,j) = N_M14';
        N_M_2{6,i}(:,j) = N_M15';
        N_M_2{7,i}(:,j) = N_M16';
        N_M_2{8,i}(:,j) = N_M17';


        if i < 4
        % Standing genetic variation: 
        N0_vec = N0_struct{1};
        Q0_Mode_sgv{1,i}(1,j) = prod(Q01v.^(N0_vec(i,:)));
        Q0_Mode_sgv{2,i}(1,j) = prod(Q02v.^(N0_vec(i,:)));
        Q0_Mode_sgv{3,i}(1,j) = prod(Q03v.^(N0_vec(i,:)));

        Q0_Mode_sgv_2{1,i}(1,j) = prod(Q010v.^(N0_vec(i,:)));
        Q0_Mode_sgv_2{2,i}(1,j) = prod(Q011v.^(N0_vec(i,:)));
        
        N0_vec = N0_struct{2};
        Q0_Mode_sgv{4,i}(1,j) = prod(Q04v.^(N0_vec(i,:)));
        Q0_Mode_sgv{5,i}(1,j) = prod(Q05v.^(N0_vec(i,:)));
        Q0_Mode_sgv{6,i}(1,j) = prod(Q06v.^(N0_vec(i,:)));
        Q0_Mode_sgv{7,i}(1,j) = prod(Q07v.^(N0_vec(i,:)));
        Q0_Mode_sgv{8,i}(1,j) = prod(Q08v.^(N0_vec(i,:)));
        Q0_Mode_sgv{9,i}(1,j) = prod(Q09v.^(N0_vec(i,:)));
        
        Q0_Mode_sgv_2{3,i}(1,j) = prod(Q012v.^(N0_vec(i,:)));
        Q0_Mode_sgv_2{4,i}(1,j) = prod(Q013v.^(N0_vec(i,:)));
        Q0_Mode_sgv_2{5,i}(1,j) = prod(Q014v.^(N0_vec(i,:)));
        Q0_Mode_sgv_2{6,i}(1,j) = prod(Q015v.^(N0_vec(i,:)));
        Q0_Mode_sgv_2{7,i}(1,j) = prod(Q016v.^(N0_vec(i,:)));
        Q0_Mode_sgv_2{8,i}(1,j) = prod(Q017v.^(N0_vec(i,:)));
        end
end
end


%==========================================================================
%% Plot Data - Figure 4 
fig2 = figure(3);
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,9]; 
T = tiledlayout(2,3);  
group_ind = [7,5,9];
col_vec2 = [dark;orange;pink];
u_ind=3;

for i=1:3
    nexttile
    plots2(2)=plot(1+c_total,Q0_Mode{group_ind(1),i},'Color',col_vec2(1,:));
    hold on
    plots2(1)=plot(1+c_total,Q0_Mode{group_ind(2),i},'Color',col_vec2(2,:));
    hold on 
    plots2(3)=plot(1+c_total,Q0_Mode{group_ind(3),i},'Color',col_vec2(3,:));
    hold on 
    set(gca,'FontSize',font1); 
    title(strs_u{i},'FontSize',font1,'Interpreter','latex'); 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    if i == 1
        leg4=legend(plots2,'\textsf{CK+S}','\textsf{CK+CK}','\textsf{S+S}','Location','southeast','Interpreter','latex');
        leg4.ItemTokenSize = [15,18];
        ylabel({'\textsf{Probability of}'; '\textsf{treatment success}'},'Interpreter','latex','FontSize',font1); 
    end
    axis tight 
    set(gca,'XScale','log');
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
end 



nexttile 
for j = 1:3 
    plotsj(j) = plot(c_total+1,p_est{group_ind(j),u_ind}(2,:),'Color',col_vec2(j,:)); 
    hold on 
    plotsj(j) = plot(c_total+1,p_est{group_ind(j),u_ind}(3,:),':','Color',col_vec2(j,:)); 
    hold on 
    plot(c_total+1,p_est{group_ind(j),u_ind}(4,:),'--','Color',col_vec2(j,:)); 
    hold on
end
plot_leg(1) = plot(nan,nan,'k','Color',0.6*ones(1,3));
plot_leg(2) = plot(nan,nan,'--','Color',0.6*ones(1,3));
set(gca,'FontSize',font1); 
% leg_m=legend(plot_leg,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m=legend(plot_leg,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m.ItemTokenSize = [15,18];
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
title('\textsf{Establishment probability}','FontSize',font1,'Interpreter','latex'); 
ylabel('$p_{\mathrm{est}}^{M_I}$');
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3) 
ylim([0,1]);

nexttile 
for j = 1:3
    plot(c_total+1,N_M{group_ind(j),u_ind}(2,:)+N_M{group_ind(j),u_ind}(3,:),'Color',col_vec2(j,:)); 
    hold on 
    plot(c_total+1,N_M{group_ind(j),u_ind}(4,:),'--','Color',col_vec2(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'YTick',[10^-5,1,10^5],'YTickLabels',{'$10^{-5}$','$10^0$','$10^{5}$'});
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylabel({'$N_{M_I}^{(\mathrm{tot})}$'}); 
title({'\textsf{Components of the approximation}, $u_i = 10^{-5}$';'\textsf{Number of de novo mutants}'},'FontSize',font1); 
text(pos(1),pos(2),charlbl{5},'Units','normalized','FontSize',font3) 
ylim([10^-6,10^8])


nexttile 
for j = 1:3
    plot(c_total+1,p_est{group_ind(j),u_ind}(2,:).*N_M{group_ind(j),2}(2,:)+p_est{group_ind(j),u_ind}(3,:).*N_M{group_ind(j),2}(3,:)+p_est{group_ind(j),u_ind}(4,:).*N_M{group_ind(j),2}(4,:),'Color',col_vec2(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'YTick',[10^-10,10^-5,1],'YTickLabels',{'$10^{-10}$','$10^{-5}$','$10^0$'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylim([10^-13,10^4])
title({'\textsf{Number of successful Mutants}'},'FontSize',font1); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 
text(pos(1),pos(2),charlbl{9},'Units','normalized','FontSize',font3) 

title(T,{'\textsf{Probability of treatment success in the absence of pre-existing mutants}, $N_{0,M_I} = 0$'},'Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1);  
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','XLim'),'XLim',[1,36]);


%% Plot Data - Figure 5 
pos=[-0.2,1.2];
col_vec = [purple;orange;green];
u_ind=3;
fig2=figure(3); 
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,13]; 
T = tiledlayout(3,3);  
ind = [2,1,3];
for i=1:3
    nexttile
    for j = 1:3
    plots(ind(j))=plot(1+c_total,Q0_Mode{3+j,i},'Color',col_vec(j,:));
    hold on 
    end
    plots(4)=plot(1+c_total,Q0_Mode{3,i},'Color',0.8*ones(1,3));
    hold on 
    set(gca,'FontSize',font1); 
    title(strs_u{i},'FontSize',font1,'Interpreter','latex'); 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    if i == 1
        leg3=legend(plots,'\textsf{CK+S}','\textsf{CR+CK}','\textsf{CR+S}','\textsf{Mono}','Location','southeast','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XScale','log');
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
end 

for i=1:3
    nexttile
    for j = 1:3
    plot(1+c_total,Q0_Mode_sgv{3+j,i},'Color',col_vec(j,:));
    hold on 
    end
     plot(1+c_total,Q0_Mode_sgv{1,i},'Color',0.8*ones(1,3));
    hold on 
    set(gca,'FontSize',font1); 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    axis tight 
    set(gca,'XScale','log');
    text(pos(1),pos(2),charlbl{3+i},'Units','normalized','FontSize',font3)
    title(strs_u{i},'FontSize',font1,'Interpreter','latex'); 
    if i == 1
    ylab4 = ylabel({'\textsf{Probability of treatment success} ($P_{\mathrm{success}}$)'},'Interpreter','latex','FontSize',font1); 
    ylab4.Position(2) = 1.2;
    end
    if i == 2
        title({'\textsf{Probability of treatment success in the presence of pre-existing mutants}, $N_{0,M_I}>0$';'$u_i = 10^{-9}$'},'FontSize',font1);
    end
end 


nexttile 
for j = 1:3
    plotsj(j) = plot(c_total+1,p_est{j+3,u_ind}(2,:),'Color',col_vec(j,:)); 
    hold on 
    plotsj(j) = plot(c_total+1,p_est{j+3,u_ind}(3,:),'-.','Color',col_vec(j,:)); 
    hold on 
    plot(c_total+1,p_est{j+3,u_ind}(4,:),'--','Color',col_vec(j,:)); 
    hold on
end
plot_leg(1) = plot(nan,nan,'k','Color',0.6*ones(1,3));
plot_leg(2) = plot(nan,nan,'k-.','Color',0.6*ones(1,3));
plot_leg(3) = plot(nan,nan,'--','Color',0.6*ones(1,3));
set(gca,'FontSize',font1); 
% leg_m=legend(plot_leg,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m=legend(plot_leg,'$M_1$','$M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m.ItemTokenSize = [15,18];
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
title('\textsf{Establishment probability}','FontSize',font1,'Interpreter','latex'); 
ylabel('$p_{\mathrm{est}}^{M_I}$');
text(pos(1),pos(2),charlbl{7},'Units','normalized','FontSize',font3) 

nexttile 
for j = 1:3
    plot(c_total+1,N_M{j+3,u_ind}(2,:)+N_M{j+3,u_ind}(3,:),'Color',col_vec(j,:)); 
    hold on 
    plot(c_total+1,N_M{j+3,u_ind}(4,:),'--','Color',col_vec(j,:)); 
    hold on 
end
plot_leg2(1) = plot(nan,nan,'k','Color',0.6*ones(1,3));
plot_leg2(2) = plot(nan,nan,'--','Color',0.6*ones(1,3));
set(gca,'FontSize',font1); 
leg_m=legend(plot_leg2,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m.ItemTokenSize = [15,18];

set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylabel({'$N_{M_I}^{(\mathrm{tot})}$'}); 
title({'\textsf{Components of the approximation}, $u_i = 10^{-5}$';'\textsf{Number of de novo mutants}'},'FontSize',font1); 
text(pos(1),pos(2),charlbl{8},'Units','normalized','FontSize',font3) 

nexttile 
for j =1:3
    plot(c_total+1,p_est{j+3,u_ind}(2,:).*N_M{j+3,2}(2,:)+p_est{j+3,u_ind}(3,:).*N_M{j+3,2}(3,:)+p_est{j+3,u_ind}(4,:).*N_M{j+3,2}(4,:),'Color',col_vec(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
title({'\textsf{Number of successful Mutants}'},'FontSize',font1); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 
text(pos(1),pos(2),charlbl{9},'Units','normalized','FontSize',font3) 

title(T,{'\textsf{Probability of treatment success in the absence of pre-existing mutants}, $N_{0,M_I} = 0$'},'Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1);  
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','XLim'),'XLim',[1,36]);


%% ========================================================================
% SI FIGURES
% 
% To generate S16: run the data generation again but replace PD1 - by PD2
% To generate S17: run the data generation again but replace PD1 - by PD3
% (see comment at the beginning of data generation section)
%% Figure S18
pos=[-0.2,1.2];
ind = [2,1,3];
col_vec = [purple;orange;green];
u_ind=2;
fig2=figure(2); 
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,13]; 
T = tiledlayout(3,3);  

for i=1:3
    nexttile
    for j = 1:3
    plots(ind(j))=plot(1+c_total,Q0_Mode_2{2+j,i},'Color',col_vec(j,:));
    hold on 
    end
    plots(4)=plot(1+c_total,Q0_Mode_2{2,i},'Color',0.8*ones(1,3));
    hold on 
    set(gca,'FontSize',font1); 
    title(strs_u{i},'FontSize',font1,'Interpreter','latex'); 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    if i == 1
        leg3=legend(plots,'\textsf{CK+S}','\textsf{CR+CK}','\textsf{CR+S}','Mono','Location','southeast','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
    end
    axis tight 
    set(gca,'XScale','log');
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
end 

for i=1:3
    nexttile
    for j = 1:3
    plot(1+c_total,Q0_Mode_sgv_2{2+j,i},'Color',col_vec(j,:));
    hold on 
    end
     plot(1+c_total,Q0_Mode_sgv{1,i},'Color',0.8*ones(1,3));
    hold on 
    set(gca,'FontSize',font1); 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    axis tight 
    set(gca,'XScale','log');
    text(pos(1),pos(2),charlbl{3+i},'Units','normalized','FontSize',font3)
    title(strs_u{i},'FontSize',font1,'Interpreter','latex'); 
    if i == 1
    ylab4 = ylabel({'\textsf{Probability of treatment success}'},'Interpreter','latex'); 
    ylab4.Position(2) = 1.2;
    end
    if i == 2
        title({'\textsf{Probability of treatment success in the presence of pre-existing mutants}, $N_{0,M_I}>0$';'$u_i = 10^{-9}$'},'FontSize',font1);
    end
   
end 


nexttile 
for j = 1:3
    plotsj(j) = plot(c_total+1,p_est_2{j+2,u_ind}(2,:),'Color',col_vec(j,:)); 
    hold on 
    plotsj(j) = plot(c_total+1,p_est_2{j+2,u_ind}(3,:),'-.','Color',col_vec(j,:)); 
    hold on 
    plot(c_total+1,p_est_2{j+2,u_ind}(4,:),'--','Color',col_vec(j,:)); 
    hold on
end
plot_leg(1) = plot(nan,nan,'k','Color',0.6*ones(1,3));
plot_leg(2) = plot(nan,nan,'k-.','Color',0.6*ones(1,3));
plot_leg(3) = plot(nan,nan,'--','Color',0.6*ones(1,3));
set(gca,'FontSize',font1); 
% leg_m=legend(plot_leg,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m=legend(plot_leg,'$M_1$','$M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m.ItemTokenSize = [15,18];
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
title('\textsf{Establishment probability}','FontSize',font1,'Interpreter','latex'); 
ylabel('$p_{\mathrm{est}}^{M_I}$');
text(pos(1),pos(2),charlbl{7},'Units','normalized','FontSize',font3) 

nexttile 
for j = 1:3
    plot(c_total+1,N_M_2{j+2,u_ind}(2,:)+N_M_2{j+2,u_ind}(3,:),'Color',col_vec(j,:)); 
    hold on 
    plot(c_total+1,N_M_2{j+2,u_ind}(4,:),'--','Color',col_vec(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylabel({'$N_{M_I}^{(\mathrm{tot})}$'}); 
title({'\textsf{Components of the approximation}, $u_i = 10^{-5}$';'\textsf{Number of de novo mutants}'},'FontSize',font1); 
text(pos(1),pos(2),charlbl{8},'Units','normalized','FontSize',font3) 
plot_leg2(1) = plot(nan,nan,'k','Color',0.6*ones(1,3));
plot_leg2(2) = plot(nan,nan,'--','Color',0.6*ones(1,3));
set(gca,'FontSize',font1); 
leg_m=legend(plot_leg2,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m.ItemTokenSize = [15,18];


nexttile 
for j =1:3
    plot(c_total+1,p_est_2{j+2,u_ind}(2,:).*N_M_2{j+2,2}(2,:)+p_est_2{j+2,u_ind}(3,:).*N_M_2{j+2,2}(3,:)+p_est_2{j+2,u_ind}(4,:).*N_M_2{j+2,2}(4,:),'Color',col_vec(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
title({'\textsf{Number of successful Mutants}'},'FontSize',font1); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 
text(pos(1),pos(2),charlbl{9},'Units','normalized','FontSize',font3) 


title(T,{'\textsf{Probability of treatment success in the absence of pre-existing mutants}, $N_{0,M_I} = 0$'},'Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1);  
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','XLim'),'XLim',[1,36]);

%% Figure S15
fig3 = figure(3);
fig3.Units = 'centimeters'; 
fig3.Position=[10,2,15.5,9]; 
T = tiledlayout(2,3);  
group_ind = [8,6,9];
col_vec2 = [green;orange;pink];

for i=1:3
    nexttile
    plots2(1)=plot(1+c_total,Q0_Mode{group_ind(1),i},'Color',col_vec2(1,:));
    hold on
    plots2(3)=plot(1+c_total,Q0_Mode{group_ind(2),i},'Color',col_vec2(2,:));
    hold on 
    plots2(2)=plot(1+c_total,Q0_Mode{group_ind(3),i},'Color',col_vec2(3,:));
    hold on 
    set(gca,'FontSize',font1); 
    title(strs_u{i},'FontSize',font1,'Interpreter','latex'); 
    set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
    if i == 1
        leg3=legend(plots2,'\textsf{CR+CR}','\textsf{S+S}','\textsf{CR+S}','Location','southeast','Interpreter','latex');
        leg3.ItemTokenSize = [15,18];
        ylabel({'\textsf{Probability of}'; '\textsf{treatment success}'},'Interpreter','latex','FontSize',font1); 
    end
    axis tight 
    set(gca,'XScale','log');
    text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',font3) 
end 



nexttile 
for j = 1:3 
    plotsj(j) = plot(c_total+1,p_est{group_ind(j),u_ind}(2,:),'Color',col_vec2(j,:)); 
    hold on 
    plotsj(j) = plot(c_total+1,p_est{group_ind(j),u_ind}(3,:),'-.','Color',col_vec2(j,:)); 
    hold on 
    plot(c_total+1,p_est{group_ind(j),u_ind}(4,:),'--','Color',col_vec2(j,:)); 
    hold on
end
plot_leg4(1) = plot(nan,nan,'k','Color',0.6*ones(1,3));
plot_leg4(2) = plot(nan,nan,'--','Color',0.6*ones(1,3));
set(gca,'FontSize',font1); 
leg_m=legend(plot_leg4,'$M_1,M_2$','$M_{1,2}$','Location','northeast','Interpreter','latex');
leg_m.ItemTokenSize = [15,18];
axis tight 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
title('\textsf{Establishment probability}','FontSize',font1,'Interpreter','latex'); 
ylabel('$p_{\mathrm{est}}^{M_I}$');
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3) 

nexttile 
for j = 1:3
    plot(c_total+1,N_M{group_ind(j),u_ind}(2,:)+N_M{group_ind(j),u_ind}(3,:),'Color',col_vec2(j,:)); 
    hold on 
    plot(c_total+1,N_M{group_ind(j),u_ind}(4,:),'--','Color',col_vec2(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'YTick',[10^-10,10^-5,1],'YTickLabels',{'$10^{-10}$','$10^{-5}$','$10^0$'});
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
ylabel({'$N_{M_I}^{(\mathrm{tot})}$'}); 
title({'\textsf{Components of the approximation}, $u_i = 10^{-5}$';'\textsf{Number of de novo mutants}'},'FontSize',font1); 
text(pos(1),pos(2),charlbl{5},'Units','normalized','FontSize',font3) 
ylim([10^-11,10^4])

nexttile 
for j = 1:3
    plot(c_total+1,p_est{group_ind(j),u_ind}(2,:).*N_M{group_ind(j),2}(2,:)+p_est{group_ind(j),u_ind}(3,:).*N_M{group_ind(j),2}(3,:)+p_est{group_ind(j),u_ind}(4,:).*N_M{group_ind(j),2}(4,:),'Color',col_vec2(j,:)); 
    hold on 
end
set(gca,'FontSize',font1); 
set(gca,'XTick',[2,11],'XTickLabels',{'1','10'});
set(gca,'YTick',[10^-10,10^-5,1],'YTickLabels',{'$10^{-10}$','$10^{-5}$','$10^0$'});
set(gca,'XScale','log'); 
set(gca,'YScale','log'); 
title({'\textsf{Number of successful Mutants}'},'FontSize',font1); 
ylabel('$\sum_I p_{\mathrm{est}}^{M_I} \cdot N_{M_I}^{\mathrm{(tot)}}$ ','Interpreter','latex','FontSize',font1); 
text(pos(1),pos(2),charlbl{6},'Units','normalized','FontSize',font3) 
ylim([10^-11,10^4])

title(T,{'\textsf{Probability of treatment success in the absence of pre-existing mutants}, $N_{0,M_I} = 0$'},'Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1);  
set(findall(fig3,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig3,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig3,'-property','XLim'),'XLim',[1,36]);


%% ========================================================================
% PD FUNCTION - S8
% =========================================================================
%% 
strs_T = {'\textsf{CK+CR}','\textsf{CK+S}','\textsf{CR+S}'}; 
PD_1 = PD1;
PD_1.psi_min = PD_1.lambda_0-PD_1.mu_0-PD_1.sigma_max*PD_1.lambda_0;
PD_1.p_max = PD_1.sigma_max/2; 
PD = [PD_1,PD_1];
p_vec = [1,0.5,0]; 
col_vec=[dark;purple;green;dark;orange;pink;green;occa;pink];
modes_j = {[1,2],[1,3],[2,3]};
pos = [-0.15,1.1];
fig1=figure(1);
T=tiledlayout(3,3);
fig1.Units = 'centimeters'; 
fig1.Position=[10,2,15.5,15];   
marker_j = {'-','-','--'};
ind= [1,3,2];
PD_1.kappa = 0.5; 
PD = [PD_1,PD_1];
for j = 1:3
    nexttile
    plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
    hold on 
    for i = 1:3
        plots_i(ind(i)) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[p_vec(i)*c,(1-p_vec(i))*c],modes_j{j}),c_total),marker_j{i},'Color',col_vec(i+(j-1)*3,:));
        hold on 
    end

    title({strs_T{j};'$\kappa_i = 0.5$'}); 
    set(gca,'FontSize',font1);
    text(pos(1),pos(2),charlbl{j},'Units','normalized','FontSize',font3) 
    if j ==1 
        leg_m=legend(plots_i,'\textsf{CK}','\textsf{CR}','\textsf{CK+CR}','Location','northeast','Interpreter','latex');
        leg_m.ItemTokenSize = [15,18];
    elseif j ==2 
        leg_m=legend(plots_i,'\textsf{CK}','\textsf{S}','\textsf{CK+S}','Location','northeast','Interpreter','latex');
        leg_m.ItemTokenSize = [15,18];
    else 
        leg_m=legend(plots_i,'\textsf{CR}','\textsf{S}','\textsf{CR+S}','Location','northeast','Interpreter','latex');
        leg_m.ItemTokenSize = [15,18];
    end
       if j < 3
         ylim([-1.5,0.7]);
     else 
         ylim([-1.5,0.7]);
       end
end

PD_1.kappa = 1; 
PD = [PD_1,PD_1];
for j = 1:3
    nexttile
    plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
    hold on 
    for i = 1:3
        plots_j(i) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[p_vec(i)*c,(1-p_vec(i))*c],modes_j{j}),c_total),marker_j{i},'Color',col_vec(i+(j-1)*3,:));
        hold on 
    end
    set(gca,'FontSize',font1);
    text(pos(1),pos(2),charlbl{j+3},'Units','normalized','FontSize',font3) 
       if j < 3
         ylim([-1.5,0.7]);
     else 
         ylim([-1.5,0.7]);
       end
       title('$\kappa_i = 1$');
end
    
PD_1.kappa = 2; 
PD = [PD_1,PD_1];
for j = 1:3
    nexttile
    plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
    hold on 
    for i = 1:3
        plots_j(i) = plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[p_vec(i)*c,(1-p_vec(i))*c],modes_j{j}),c_total),marker_j{i},'Color',col_vec(i+(j-1)*3,:));
        hold on 
    end
    set(gca,'FontSize',font1);
     text(pos(1),pos(2),charlbl{j+6},'Units','normalized','FontSize',font3) 
     if j < 3
         ylim([-1.5,0.7]);
     else 
         ylim([-1.5,0.7]);
     end
       title('$\kappa_i = 2$');
end

title(T,'\textsf{Drugs with di{f}{f}erent modes of action}','Interpreter','latex','FontSize',font2)
xlabel(T,'\textsf{Total drug concentration in multiples of} $\mathrm{zMIC}_W$','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Net growth rate} $\psi$','Interpreter','latex','FontSize',font1);
set(findall(fig1,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig1,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig1,'-property','XLim'),'XLim',[0,8]);
set(findall(fig1,'-property','YLim'),'YLim',[-1.2,0.7]);

