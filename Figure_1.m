% This skript produces the figure displayed in the method section and the
% additional figures used in the Supplement: 

% - Figure 1 
% - Figures S1-S3
clc 
clear 
close all


%% 1. Plot Settings

dark = [2 93 114]/255;
pink = [178 93 141]/255;
light = [121 182 185]/255; 
occa = [215,200,100]/255;
green = [150,180,130]/255; 
orange = [200,130,60]/255; 
grey = [150,150,150]/255; 
occa2 = [236,177,32]/255;
lines_col = lines(7);
purple = lines_col(4,:)+0;
% Define a color vector 
col_vec = [dark;pink;light;occa;0.2*ones(1,3)];
set(0,'DefaultLineLineWidth',1.5);  
marker_type = {'-.','-','-.','-','-.','--','-'}; 
mymap = [dark;green;pink;orange;purple;occa2;grey];
n = 4; 
mymap2 = [linspace(0.2,0.8,4)', linspace(0.2,0.8,4)',linspace(0.2,0.8,4)'];

set(0,'DefaultLineMarkerSize',5); 

%For labeling the panels 
nIDs = 9;
alphabet = ('A':'Z').';
chars = num2cell(alphabet(1:nIDs));
chars = chars.';
charlbl = strcat('(',chars,')'); % {'(a)','(b)','(c)','(d)'}
pos = [0.01,1.1]; 
% then use: 
% text(pos(1),pos(2),charlbl{i},'Units','normalized','FontSize',12) 

% Fonts
font1 = 9; 
font2 = 11;
font3 = 9; 
%% 2. Parameters: 

% PD Parameter (1x9 struct array) 
% First drug 
PD1.lambda_0 = 0.7;   % replication rate 
PD1.mu_0 = 0.1;        % intrinsic death rate 
PD1.psi_min = -1;     % Psi_min for the first drug 
PD1.kappa = 1;          % Kappa for the first drug 
PD1.zMIC = 1;      % zMIC for the first drug 
PD1.cost = 0.1;         % cost per mutation 
PD1.benefit = 28;       % benefit of mutation 
PD1.p_max = 0.95;        % p_max value for cdial drug action coupled with replication
PD1.sigma_max = 0.95;    % sigma_max value for static drug action 

% Second drug e.g. with different kappa value 
PD2 = PD1; 
PD2.kappa = 2; 

% Third drug e.g. with different kappa value 
PD3 = PD1; 
PD3.kappa = 0.5; 

% Gerenal parameter and vectors for figures 
u_vec = [10^-11,10^-9,10^-5];
N0 = 10^10;
strs = {'$u_A = u_B = 10^{-11}$','$u_A = u_B = 10^{-9}$','$u_A = u_B = 10^{-5}$'}; 
c_total = linspace(0,10,1000); 

% For growth rates of different types 
PD2res = PD2; 
PD2res.zMIC = PD2.zMIC*5;
PD3res = PD3; 
PD3res.zMIC = PD3.zMIC*5;

%% 3. Figure 1

fig2=figure(2);
T=tiledlayout(2,2);
T.TileSpacing = 'compact';
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,10];   
mode = 1*ones(1,n);
% kappa = 2
PD = repmat(PD2,[1 n]); 
% PD = [PD2res,repmat(PD2,[1 n-1])]; 
% PD = [PD2res,PD2res,repmat(PD2,[1 n-2])]; 


nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title('$\kappa_i = 2$','Interpreter','latex','FontSize',font1); 
set(gca,'YTick',[-1,0,0.6],'YTickLabels',{'$\psi_{\rm{min}}$','0','$\lambda_0^W-\mu_0$'});
set(gca,'XTick',[0,1,5,10],'XTickLabels',[]);
text(pos(1),pos(2),charlbl{1},'Units','normalized','FontSize',font3)

nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title('$\kappa_i = 2$','Interpreter','latex','FontSize',font1); 
set(gca,'XTick',[0,1,5,10],'XTickLabels',[]);
set(gca,'YTick',[-1,0,0.6],'YTickLabels',[]);
text(pos(1),pos(2),charlbl{2},'Units','normalized','FontSize',font3)

% kappa = 0.5
PD = repmat(PD3,[1 n]); 
% PD = [PD3res,repmat(PD3,[1 n-1])]; 
% PD = [PD3res,PD3res,repmat(PD3,[1 n-2])]; 
nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plots_m(i) = plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title({'$\kappa_i = 0.5$'},'Interpreter','latex','FontSize',font1); 
set(gca,'XTick',[0,1,5,10]);
set(gca,'YTick',[-1,0,0.6],'YTickLabels',{'$\psi_{\rm{min}}$','0','$\lambda_0^W-\mu_0$'});
leg = legend(plots_m,{'$\psi_W(c)$','$\psi_W(\frac{c}{2},\frac{c}{2})$','$\psi_W(\frac{c}{3},\frac{c}{3},\frac{c}{3})$','$\psi_W(\frac{c}{4},\frac{c}{4},\frac{c}{4},\frac{c}{4})$'},'Location','southwest');
leg.ItemTokenSize = [15 18];
text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3) 

nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1); 
title({'$\kappa_i = 0.5$'},'Interpreter','latex','FontSize',font1); 
set(gca,'YTick',[-1,0,0.6],'YTickLabels',[]);
set(gca,'XTick',[0,1,5,10]);
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3)


title(T,'\textsf{Loewe additivity Bliss independence}','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of $\mathrm{zMIC}_W$}','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Net growth rate} $\psi_W$','Interpreter','latex','FontSize',font1);
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','XLim'),'XLim',[0,10]);
set(findall(fig2,'-property','YLim'),'YLim',[-4,0.7]);

%% 4. Figure S1
% Other modes of action Loewe - kappa=0.5 and 2 for smax and pmax = 1
fig2=figure(2);
T=tiledlayout(2,2);
T.TileSpacing = 'compact';
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,10];   
mode = 1*ones(1,n);

% kappa = 2
PD2.sigma_max = 1; 
PD2.p_max = 1; 
PD = repmat(PD2,[1 n]); 

nexttile(1)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD,[c/i*ones(1,i),zeros(1,n-i)],2*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title('$\kappa_i = 2$','FontSize',font1);
set(gca,'YTick',[-0.8,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{CR}}$','0','$\lambda_0^W-\mu_0$'});
set(gca,'XTick',[0,1,5,10],'XTickLabels',[]);
text(pos(1),pos(2),charlbl{1},'Units','normalized','FontSize',font3)
ylim([-1,0.7])
xlim([0,10]);

nexttile(2)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD,[c/i*ones(1,i),zeros(1,n-i)],3*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title('$\kappa_i = 2$','FontSize',font1);
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{S}}$','0','$\lambda_0^W-\mu_0$'});
set(gca,'XTick',[0,1,5,10],'XTickLabels',[]);
text(pos(1),pos(2),charlbl{2},'Units','normalized','FontSize',font3)
ylim([-0.2,0.7])
xlim([0,10]);

PD3.sigma_max = 1; 
PD3.p_max = 1; 
PD = repmat(PD3,[1 n]); 

nexttile(3)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plots_m(i)=plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD,[c/i*ones(1,i),zeros(1,n-i)],2*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title({'$\kappa_i = 0.5$'},'FontSize',font1);
set(gca,'YTick',[-0.8,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{CR}}$','0','$\lambda_0^W-\mu_0$'});
set(gca,'XTick',[0,1,5,10]);
text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3)
ylim([-1,0.7])
xlim([0,10]);


nexttile(4)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plots_m(i)=plot(c_total, arrayfun(@(c) psi_n_Drugs_LOEWE(PD,[c/i*ones(1,i),zeros(1,n-i)],3*mode),c_total),'Color',mymap(i,:));
    hold on 
end
leg = legend(plots_m,{'$\psi_W(c)$','$\psi_W(\frac{c}{2},\frac{c}{2})$','$\psi_W(\frac{c}{3},\frac{c}{3},\frac{c}{3})$','$\psi_W(\frac{c}{4},\frac{c}{4},\frac{c}{4},\frac{c}{4})$'},'Location','northeast');
leg.ItemTokenSize = [15 18];
set(gca,'FontSize',font1);
title({'$\kappa_i = 0.5$'},'FontSize',font1);
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{S}}$','0','$\lambda_0^W-\mu_0$'});
set(gca,'XTick',[0,1,5,10]);
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3)
ylim([-0.2,0.7])
xlim([0,10]);


title(T,'\textsf{Bactericidal (CR) Bacteriostatic (S)}','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of $\mathrm{zMIC}_W$}','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Net growth rate} $\psi_W$','Interpreter','latex','FontSize',font1);
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
% set(findall(fig2,'-property','XLim'),'XLim',[0,10]);
% set(findall(fig2,'-property','YLim'),'YLim',[-1,0.7]);


%% 5- Figure S2 
% Other modes of action Bliss - kappa=0.5 and 2 for smax and pmax = 1
fig2=figure(2);
T=tiledlayout(2,2);
T.TileSpacing = 'compact';
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,10];   
mode = 1*ones(1,n);

% kappa = 2
PD2.sigma_max = 1; 
PD2.p_max = 1; 
PD = repmat(PD2,[1 n]); 

nexttile(1)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],2*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title({'$\kappa_i = 2$'},'FontSize',font1);
set(gca,'YTick',[-0.8,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{CR}}$','0','$\lambda_0^W-\mu_0$'});
text(pos(1),pos(2),charlbl{1},'Units','normalized','FontSize',font3)
set(gca,'XTick',[0,1,5,10],'XTickLabels',[]);
ylim([-1,0.7])
xlim([0,10]);

nexttile(2)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],3*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title({'$\kappa_i = 2$'},'FontSize',font1);
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{S}}$','0','$\lambda_0^W-\mu_0$'});
text(pos(1),pos(2),charlbl{2},'Units','normalized','FontSize',font3)
set(gca,'XTickLabels',[]);
ylim([-0.2,0.7])
xlim([0,3]);

PD3.sigma_max = 1; 
PD3.p_max = 1; 
PD = repmat(PD3,[1 n]); 

nexttile(3)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plots_m(i)=plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],2*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
title({'$\kappa_i = 0.5$'},'FontSize',font1);
set(gca,'YTick',[-0.8,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{CR}}$','0','$\lambda_0^W-\mu_0$'});
text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3)
ylim([-1,0.7])
set(gca,'XTick',[0,1,5,10]);
xlim([0,10]);


nexttile(4)
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plots_m(i)=plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],3*mode),c_total),'Color',mymap(i,:));
    hold on 
end
leg = legend(plots_m,{'$\psi_W(c)$','$\psi_W(\frac{c}{2},\frac{c}{2})$','$\psi_W(\frac{c}{3},\frac{c}{3},\frac{c}{3})$','$\psi_W(\frac{c}{4},\frac{c}{4},\frac{c}{4},\frac{c}{4})$'},'Location','northeast');
leg.ItemTokenSize = [15 18];
set(gca,'FontSize',font1);
title({'$\kappa_i = 0.5$'},'FontSize',font1);
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}^{\mathrm{S}}$','0','$\lambda_0^W-\mu_0$'});
text(pos(1),pos(2),charlbl{4},'Units','normalized','FontSize',font3)
ylim([-0.2,0.7])
xlim([0,3]);

title(T,'\textsf{Bactericidal (CR) Bacteriostatic (S)}','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of $\mathrm{zMIC}_W$}','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Net growth rate} $\psi_W$','Interpreter','latex','FontSize',font1);
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
% set(findall(fig2,'-property','XLim'),'XLim',[0,10]);
% set(findall(fig2,'-property','YLim'),'YLim',[-1,0.7]);

%% 6. Figure S3
% high psi min values where mono therapy is not best

fig2=figure(2);
T=tiledlayout(1,3);
fig2.Units = 'centimeters'; 
fig2.Position=[10,2,15.5,6];   
mode = 1*ones(1,n);
pos=[-0.15,1];

% kappa = 2
PD2.sigma_max = 0.95; 
PD2.p_max = 0.95/2;
PD2.psi_min = -0.065;
PD = repmat(PD2,[1 n]); 

nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],1*mode),c_total),'Color',mymap(i,:));
    hold on 
end
set(gca,'FontSize',font1);
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',{'$\psi_{\mathrm{min}}$','0','$\lambda_0^W-\mu_0$'});
text(pos(1),pos(2),charlbl{1},'Units','normalized','FontSize',font3)
ylim([-0.2,0.7])



nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],2*mode),c_total),'Color',mymap(i,:));
    hold on 
end 
set(gca,'FontSize',font1);
text(pos(1),pos(2),charlbl{2},'Units','normalized','FontSize',font3)
ylim([-0.2,0.7])
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',[]);


nexttile
plot([0,35],[0,0],'--','Color',0.2*ones(1,4))
hold on 
for i = 1:n
    plots_m(i)=plot(c_total, arrayfun(@(c) psi_action_multiple_drugs(PD,[c/i*ones(1,i),zeros(1,n-i)],3*mode),c_total),'Color',mymap(i,:));
    hold on 
end
leg = legend(plots_m,{'$\psi_W(c)$','$\psi_W(\frac{c}{2},\frac{c}{2})$','$\psi_W(\frac{c}{3},\frac{c}{3},\frac{c}{3})$','$\psi_W(\frac{c}{4},\frac{c}{4},\frac{c}{4},\frac{c}{4})$'},'Location','northeast');
leg.ItemTokenSize = [15 18];
set(gca,'FontSize',font1);
text(pos(1),pos(2),charlbl{3},'Units','normalized','FontSize',font3)
ylim([-0.2,0.7])
set(gca,'YTick',[-0.1,0,0.6],'YTickLabels',[]);

title(T,'\textsf{Bactericidal (CK) Bactericidal (CR) Bacteriostatic (S)}','Interpreter','latex','FontSize',font2);
xlabel(T,'\textsf{Total drug concentration in multiples of $\mathrm{zMIC}_W$}','Interpreter','latex','FontSize',font1); 
ylabel(T,'\textsf{Net growth rate} $\psi_W$','Interpreter','latex','FontSize',font1);
set(findall(fig2,'-property','Interpreter'),'Interpreter','latex');
set(findall(fig2,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex');
set(findall(fig2,'-property','XLim'),'XLim',[0,2]);


