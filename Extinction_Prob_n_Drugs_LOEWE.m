function [Q,Qi_out] = Extinction_Prob_n_Drugs_LOEWE(PD,u,c,mode)
% Function to calculate the extinction probability of combination
% therapy with constant drug concentration for variable number of drugs
% based on LOEWE ADDITIVITY: 
% 
% 
% INPUT 
% PD      = nx9 struct array discribing the PD charavteristics of the
%            drugs and the different types (W MA MB MC MAB MAC MBC MABC)
%            lambda_0 - replication rate 
%            mu_0     - intrinsic death rate 
%            zMIC - susceptibility towards the drug 
%            kappa - steepness of the pharamcodynamic function 
%            psi_min - max kill rate of bactericidal (kill) drug
%            p_max - max drug effect of bactericidal (growth) drug 
%            sigma_max - max drug effect of bacteriostatic drug 
%            benefit - benefit of resistance
%            cost - cost of resistance 
% c        = 1xndrug concnetration for the two drugs
% mode     = 1xn mode of action (e.g. 1-3 as above) 
% u     = 1xn vector with mutation probabilities (uA,uB)
% 
% OUTPUT 
% P_ext = extinction probability of the process 
%
%==========================================================================
% Number of drugs 
nd = length(c); 
% Number of Types 
nt = 2^(nd); 
% Initialize Vector for Extinction probabilities starting with different
% types 
Qi = zeros(1,nt); 

%==========================================================================
% FULLY RESISTANT TYPE
%==========================================================================
% Calculate the birth and death probabilities 
[rho_ti,delta_ti] = birth_death_prob(PD,c,mode,ones(1,nd));

Qti_1 = 1/(2*rho_ti) + sqrt((1/(2*rho_ti))^2-(delta_ti)/rho_ti);
Qti_2 = 1/(2*rho_ti) - sqrt((1/(2*rho_ti))^2-(delta_ti)/rho_ti);
Qti = min([Qti_1,Qti_2,1]);

% save in array 
Qi(1,1) = Qti; 

%==========================================================================
% OTHER TYPES 
%==========================================================================

% Generate matrix with all possible resistant types (binary matrix) 
MT_base = ff2n(nd); 
MT = []; 
% order matrix elements 
for i = nd:-1:0
    ind_vec = sum(MT_base,2)==i; 
    MT = [MT;MT_base(ind_vec,:)];
end

for i = 2:nt
    % Calculate the rates and probabilities for birth and death 
    [rho_ti,delta_ti] = birth_death_prob(PD,c,mode,MT(i,:));
    
    % Calculate p and q for pq-formular: 
    q = delta_ti/(rho_ti*prod(1-u((MT(1,:)-MT(i,:))==1))); 
    
    % find all possible mutants that can be generates: 
    n_res = sum(MT(i,:)); 
    % all types with more mutations 
    ind_more_res = sum(MT,2)>n_res; 
    MT_possible_cand = MT(ind_more_res,:); 
    [ncand,~] = size(MT_possible_cand); 
    MT_possible = []; 
    % making sure every candidtate mutatnt has the mutation of the type we
    % are considering here.
    for k=1:ncand
        diff= MT_possible_cand(k,:)-MT(i,:); 
        if ~any(diff<0)
            MT_possible = [MT_possible;MT_possible_cand(k,:)];
        end
            
    end
    
    % calculate the sum of all products of extinction probability of the
    % possible mutants and their mutation rate
    
    sum_vals = [];   
    [nM,~] = size(MT_possible); 
    for j=1:nM
        new_mut = MT_possible(j,:)-MT(i,:); 
        not_mut = ~MT_possible(j,:); 
      
        ind_q = find(ismember(MT,MT_possible(j,:),'rows')); 
        if sum(not_mut == 1) > 0
            sum_vals(1,j) = Qi(1,ind_q)*prod(u(1,new_mut==1))*prod(1-u(not_mut));
        else 
            sum_vals(1,j) = Qi(1,ind_q)*prod(u(1,new_mut==1));
        end
    end
    
    p = (rho_ti*sum(sum_vals)-1)/(rho_ti*prod(1-u((MT(1,:)-MT(i,:))==1)));
    
    % Solve pg-formular
    Qti_1 = -p/2 + sqrt((p/2)^2-q); 
    Qti_2 = -p/2 - sqrt((p/2)^2-q); 

    % Exinction probability is smallest root in [0,1]
    Qti = min([Qti_1,Qti_2,1]); 
    Qi(1,i)=Qti; 
end

Q = Qi(end); 
% output all Probabilities 
Qi_out = fliplr(Qi); 
end

function [rho,delta] = birth_death_prob(PD,c,mode,ti_vec)
% Function to calulate the brith and death rates and their respective
% probabilities for the type with vec ti_vec 
nd = length(c);

    PD_temp = PD; 
    % Implement growth costs: 
    lambda_new = PD(1).lambda_0; 
    for i = 1:nd
        if ti_vec(i) == 1
            lambda_new = lambda_new*(1-PD_temp(i).cost); 
        end
    end
    % Update lambda_0 in every struct array and zMIC value 
    for i = 1:nd 
        PD_temp(i).lambda_0 = lambda_new; 
        if ti_vec(i) == 1
            PD_temp(i).zMIC = PD(i).zMIC*PD(i).benefit;
        end
    end

    % Calculate the brith and death rates
    [~,r0_ti,di_ti] = psi_LOEWE_n_Drugs(PD_temp,c,mode);
    % Probabilities 
    rho = r0_ti/(r0_ti+di_ti);
    delta = di_ti/(r0_ti+di_ti);
end

function [psi,lambda,mu] = psi_LOEWE_n_Drugs(PD,c,mode)
% Function to calculate the growth rate for the treatment with n different
% drugs under the assumption of Loewe additivity 
% Only Bactericidal drug effect on the kill rate 
% 
% INPUT 
% PD       = 1xn struct array discribing the PD charavteristics of the drugs
%            lambda_0 - replication rate (equal for all drugs)
%            mu_0     - intrinsic death rate (equal for all drugs) 
%            zMIC - susceptibility towards the drug 
%            kappa - steepness of the pharamcodynamic function 
%            psi_min - max kill rate of bactericidal (kill) drug
%            p_max - max drug effect of bactericidal (growth) drug 
%            sigma_max - max drug effect of bacteriostatic drug 
% c        = 1xn drug vector of drug concnetration for all drugs
% 
% OUTPUT 
% psi      = growth rate of the bacterial population 
% 
%==========================================================================
nd = length(c);

% define function handles for both drug PDs
% differentiate between the differet modes of action: 
if mode(1,1) == 1 % Bactericidal independent of replication 
    psi = @(c,PDi) (PDi.lambda_0-PDi.mu_0)-(PDi.lambda_0-PDi.mu_0-PDi.psi_min)*(c/PDi.zMIC).^PDi.kappa./((c/PDi.zMIC).^PDi.kappa-PDi.psi_min/(PDi.lambda_0-PDi.mu_0)); 
    inv_psi =  @(fc,PDi) ((PDi.psi_min*(PDi.lambda_0-PDi.mu_0-fc))./((PDi.lambda_0-PDi.mu_0)*(PDi.psi_min-fc))).^(1/PDi.kappa)*PDi.zMIC; 
elseif mode(1,1) == 2 % Bactericidal dependent of replication
    psi = @(c,PDi) (PDi.lambda_0-PDi.mu_0)-(PDi.lambda_0*2*PDi.p_max*(c/PDi.zMIC).^PDi.kappa)./((c/PDi.zMIC).^PDi.kappa-1+(2*PDi.p_max*PDi.lambda_0)/(PDi.lambda_0-PDi.mu_0)); 
    inv_psi = @(fc,PDi) PDi.zMIC*(((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*2*PDi.p_max)*(PDi.lambda_0-PDi.mu_0-fc))/((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*2*PDi.p_max-fc)*(PDi.lambda_0-PDi.mu_0))).^(1/PDi.kappa);
elseif mode(1,1) == 3 %Becteriostatic
    psi = @(c,PDi) (PDi.lambda_0-PDi.mu_0)-(PDi.lambda_0*PDi.sigma_max*(c/PDi.zMIC).^PDi.kappa)./((c/PDi.zMIC).^PDi.kappa-1+(PDi.sigma_max*PDi.lambda_0)/(PDi.lambda_0-PDi.mu_0)); 
    inv_psi = @(fc,PDi) PDi.zMIC*(((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*PDi.sigma_max)*(PDi.lambda_0-PDi.mu_0-fc))/((PDi.lambda_0-PDi.mu_0-PDi.lambda_0*PDi.sigma_max-fc)*(PDi.lambda_0-PDi.mu_0))).^(1/PDi.kappa);
end

% Check if kappa values are the same: 
for i = 2:nd 
    temp(i-1) = (PD(1).kappa == PD(i).kappa); 
end

if nd == 1
    temp = 1; 
end

if ~any(temp==0)
    x_eq = 0; 
    for i = 2:nd 
            x_temp = inv_psi(psi(c(1,i),PD(i)),PD(1)); 
            x_eq = x_eq+x_temp; 
    end
    psi_val = psi(c(1,1)+x_eq,PD(1)); 
else 
    % calculate the equivalent concentrations each drug and then use the mean: 
    for j = 1:nd
        x_eq = 0; 
        for i = 1:nd 
            if j ~=i
                x_temp = inv_psi(psi(c(1,i),PD(i)),PD(j)); 
                x_eq = x_eq+x_temp; 
            end
        end
    %Calculate the growth rate according to loewe 
    f(j) = psi(c(1,j)+x_eq,PD(j)); 
    end
    psi_val = mean(f); 
end

if mode(1,1) == 1
    lambda = PD(1).lambda_0; 
    mu = abs(psi_val-lambda); 
elseif mode(1,1) == 2
    pcl = abs(psi_val-(PD(1).lambda_0-PD(1).mu_0)); 
    lambda = PD(1).lambda_0-pcl/2;
    mu = PD(1).mu_0+pcl/2; 
elseif mode(1,1) == 3
    mu = PD(1).mu_0; 
    lambda = abs(psi_val+mu); 
end 

end
